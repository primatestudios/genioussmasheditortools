using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public static class EditorFileHelper
{
    static string ConvertPathRelativeToAssets(string pathRelativeToAssets)
    {
        string path = Application.dataPath + "/" + pathRelativeToAssets;
        return path.Replace('/', Path.DirectorySeparatorChar);
    }
    public static void WriteFile(string filePathRelativeToAssets, string text)
    {
        File.WriteAllText(filePathRelativeToAssets, text, Encoding.UTF8);
    }

    public static string ReadFile(string filePathRelativeToAssets)
    {
        return File.ReadAllText(ConvertPathRelativeToAssets(filePathRelativeToAssets));
    }
}
