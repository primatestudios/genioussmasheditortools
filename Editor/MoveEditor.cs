using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using static Smash_Forge.ATKD;

public class MoveEditor : EditorWindow
{

    [Serializable]
    public class EasyMoveEditorContainer
    {
        public List<EasyMoveEditor> value;
        public bool triggerReset;

        public EasyMoveEditorContainer(List<EasyMoveEditor> val)
        {
            value = val;
        }
    }
    public class EasyMoveEditor
    {
        public string moveName;
        public List<string> names = new List<string>();
        public string apiId;
        public int faf;
        public List<Move> moves;
        public HitboxEntry hitbox;
        public bool display;
        public HashSet<AnimatorState> states;
        public void Propagate(FighterData fd, bool updateStateSpeeds=false)
        {
            hitbox.startFrame = (ushort) Mathf.Max(Mathf.Min(hitbox.startFrame, faf - 2), 1);
            hitbox.lastFrame = (ushort) Mathf.Max(Mathf.Min(hitbox.lastFrame, faf - 1), hitbox.startFrame + 1);
            foreach (var m in fd.GetMoves(apiId))
            {
                m.hitbox = hitbox;
                m.hitboxActive[0].start = (float)hitbox.startFrame;
                m.hitboxActive[0].end = (float)hitbox.lastFrame;
                m.faf = faf;
                if (updateStateSpeeds && states != null)
                {
                    foreach(var s in states)
                    {
                        SmashCharacterAnimatorTool.UpdateStateSpeed(s, m.faf - 1);
                    }
                }
            }
        }

        public void DeleteMove(FighterData fd)
        {
            if ( EditorUtility.DisplayDialog("Delete " + moveName,
                "Are you sure you want to delete " + moveName + "? There is no undo.", 
                "Delete", "Cancel"))
            {
                for (int i = 0; i < fd.moves.Count; i++)
                {
                    var move = fd.moves[i];
                    if (move.api_id == apiId)
                    {
                        fd.moves.RemoveAt(i--);
                    }
                }
            }
        }
    }
    [MenuItem("Genious/Move Editor")]
    public static void ShowWindow()
    {
        GetWindow<MoveEditor>(false, "Move Editor", true);
    }

    FighterData fighterData;
    Vector2 scrollPosition;
    bool newMove = false;
    bool editFighterMoves = true;
    bool showFighterData = true;
    EasyMoveEditorContainer container;
    public void OnGUI()
    {
        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
        bool wasNull = fighterData == null || container == null || container.value == null || container.triggerReset;
        fighterData = EditorGUILayout.ObjectField("Fighter Data", fighterData, typeof(FighterData)) as FighterData;
        if (fighterData != null)
        {
            editFighterMoves = EditorGUILayout.Foldout(editFighterMoves, "Edit Fighter Moves");
            
            if (wasNull || editFighterMoves && GUILayout.Button("Refresh"))
            {
                var dict = new Dictionary<string, EasyMoveEditor>();
                foreach(var m in fighterData.moves)
                {
                    if (!dict.ContainsKey(m.api_id))
                    {
                        dict[m.api_id] = new EasyMoveEditor();
                        dict[m.api_id].moves = new List<Move>();
                        dict[m.api_id].moveName = m.moveName;
                        dict[m.api_id].hitbox = m.hitbox;
                        dict[m.api_id].apiId = m.api_id;
                        dict[m.api_id].faf = m.faf;
                        var c1 = SmashCharacterAnimatorTool.GetAnimatorStates((fighterData.setupAnimatorController as AnimatorController).layers[0].stateMachine);
                        var c2 = c1.Union(SmashCharacterAnimatorTool.GetAnimatorStates((fighterData.runtimeAnimatorController as AnimatorController).layers[0].stateMachine));
                        dict[m.api_id].states = new HashSet<AnimatorState>(c2);
                        dict[m.api_id].names = new List<string>();

                    }

                    dict[m.api_id].moves.Add(m);
                    dict[m.api_id].names.Add(m.name);
                }

                container = new EasyMoveEditorContainer(new List<EasyMoveEditor>(dict.Values));
            }
            if (editFighterMoves)
            {
                foreach (var v in container.value.OrderBy(x => x.moveName))
                {
                    EditorGUI.indentLevel++;
                    GUILayout.BeginHorizontal();
                    v.display = EditorGUILayout.Foldout(v.display, v.moveName);
                    if (GUILayout.Button("X"))
                    {
                        v.DeleteMove(fighterData);
                        container.triggerReset = true;
                        GUILayout.EndHorizontal();
                        break;
                        Debug.Log("Del");
                    }
                    GUILayout.EndHorizontal();
                    if (v.display)
                    {
                        EditorGUI.indentLevel++;
                        var printName = "";
                        foreach(var n in v.names)
                        {
                            printName += "," + n;
                        }
                        EditorGUILayout.LabelField("Name(s): {" + printName.Substring(1) + "}");
                        EditorGUILayout.LabelField("API ID: " + v.apiId);
                        EditorGUILayout.LabelField("Animation ID: " + v.hitbox.animationId);
                        Vector2 range = new Vector2(v.hitbox.startFrame, v.hitbox.lastFrame);

                        float start = v.hitbox.startFrame;
                        float end = v.hitbox.lastFrame;
                        int newFaf = EditorGUILayout.IntField("Duration (Frames):", v.faf-1);
                        int? changeFafTo = null;// = false;
                        if (newFaf != v.faf-1)
                        {
                            changeFafTo = newFaf + 1;
                        }

                        range.x = Mathf.Max(Mathf.Min(range.x, v.faf-2), 1);
                        range.y = Mathf.Max(Mathf.Min(range.y, v.faf - 1), range.x+1);

                        float numSeconds = ((float)v.faf - 1f) / 60f;
                        float duration = EditorGUILayout.FloatField("Duration(Secs): ", numSeconds);
                        if (duration != numSeconds)
                        {
                            changeFafTo = Mathf.RoundToInt(duration * 60f + 1f);
                        }

                        if (changeFafTo.HasValue)
                        {
                            v.hitbox.startFrame = (ushort) Mathf.RoundToInt((float)v.hitbox.startFrame / (float)v.faf * changeFafTo.Value);
                            v.hitbox.lastFrame = (ushort)Mathf.RoundToInt((float)v.hitbox.lastFrame / (float)v.faf * changeFafTo.Value);

                            v.faf = changeFafTo.Value;
                            v.Propagate(fighterData, true);

                        }
                        EditorGUILayout.MinMaxSlider("Hitbox Active Range", ref start, ref end, 1, v.faf - 1);
                        var newRange = EditorGUILayout.Vector2Field("Hitbox Active", range);
                        if (newRange != range)
                        {
                            v.hitbox.startFrame = (ushort)Mathf.RoundToInt(newRange.x);
                            v.hitbox.lastFrame = (ushort)Mathf.RoundToInt(newRange.y);
                            v.Propagate(fighterData);
                        }
                        else if (start != (float)v.hitbox.startFrame || end != (float)v.hitbox.lastFrame)
                        {
                            v.hitbox.startFrame = (ushort)start;
                            v.hitbox.lastFrame = (ushort)end;
                            v.Propagate(fighterData);
                        }

                        var newRect = EditorGUILayout.RectField("Hitbox position", v.hitbox.rect);
                        if (newRect != v.hitbox.rect)
                        {
                            v.hitbox.rect = newRect;
                            v.Propagate(fighterData);
                        }
                        EditorGUI.indentLevel--;
                    }
                    EditorGUI.indentLevel--;
                }

                //var serializedObject = new SerializedObject(container);
                //var myArrayProperty = serializedObject.FindProperty("value");
                //var myElement = myArrayProperty.GetArrayElementAtIndex(42);
                //myElement.objectReferenceValue = EditorGUILayout.ObjectField(myElement.objectReferenceValue, typeof(SomeType), true);
                //EditorGUILayout.PropertyField(myArrayProperty, true);
                //Editor editor = FighterEditor.GetEditor(fighterData);
                /*Editor editor = FighterEditor.GetEditor(container);
                editor.OnInspectorGUI();*/
            }

            showFighterData = EditorGUILayout.Foldout(showFighterData, "Show Fighter Data");
            if (showFighterData)
            {
                Editor editor = FighterEditor.GetEditor(fighterData);
                //Editor editor = FighterEditor.GetEditor(container);
                editor.OnInspectorGUI();
            }
        }

        if (GUILayout.Button("Save Hitbox Data"))
        {
            if (fighterData.hitboxData == null)
            {
                Debug.LogError("Not Hitbox Data available!");
            }
            if (fighterData.atkdAsset == null)
            {
                Debug.LogError("You must assign an atkdAsset in order to Save the hitbox data");
            }
            else
            {
                for (int i = 0; i < fighterData.hitboxData.entries.Count; i++)
                {
                    var entry = fighterData.hitboxData.entries[i];
                    foreach(var m in fighterData.moves)
                    {
                        if (m.AnimationId == entry.animationId)
                        {
                            entry = m.hitbox;
                        }
                    }
                    fighterData.hitboxData.entries[i] = entry;
                }

                fighterData.hitboxData.Save(fighterData.atkdAsset);
            }
        }

        if (GUILayout.Button("Update/Revert Hitbox Data\nRestore From ATKD Asset"))
        {
            if (fighterData.atkdAsset == null)
            {
                Debug.LogError("You must assign an atkdAsset in order to generate the hitbox data automatically");
            }
            else
            {
                fighterData.hitboxData = new Smash_Forge.ATKD(fighterData.atkdAsset);
                foreach (var m in fighterData.moves)
                {
                    foreach (var hd in fighterData.hitboxData.entries)
                    {
                        if (m.AnimationId == hd.animationId && m.AnimationId != 0)
                        {
                            hd.startFrame = ushort.Parse(m.hitboxActive[0].start.ToString());
                            hd.lastFrame = ushort.Parse(m.hitboxActive[0].end.ToString());
                            m.hitbox = hd;
                        }
                    }
                }
            }
        }

        if (GUILayout.Button("Generate Moves From Unparsed"))
        {
            fighterData.moves = new List<Move>();
            foreach(var m in fighterData.unparsedMoves)
            {
                var newMoves = Move.GetMoves(m, fighterData);
                foreach(var nm in newMoves)
                {
                    nm.unparsedMove = m;
                }
                fighterData.moves.AddRange(newMoves);
            }
        }
        EditorGUILayout.EndScrollView();

    }
}
 