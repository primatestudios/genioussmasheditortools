using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class AnimationClipEditor : EditorWindow
{
    [MenuItem("Genious/Animation Clip Editing Shortcuts")]
    public static void ShowWindow()
    {
        GetWindow<AnimationClipEditor>(false, "Animation Clip Editor", true);
    }

    static string pathToCopy;
    static string animationClipName;
    static string copyTargetLocation;
    [UnityEditor.MenuItem("Assets/Duplicate Clip For Edits")]
    private static void UpdateRuntimeControllerAssetMenu()
    {
        EditorApplication.ExecuteMenuItem("Edit/Duplicate");
        AssetDatabase.Refresh();
        string fileName = animationClipName + ".anim";
        AssetDatabase.MoveAsset(pathToCopy + fileName, copyTargetLocation + "/" + fileName);
        AssetDatabase.Refresh();
    }

    [UnityEditor.MenuItem("Assets/Duplicate Clip For Edits", true)]
    private static bool DuplicateClipsForEditsValidation()
    {
        if (UnityEditor.Selection.activeObject != null && UnityEditor.Selection.activeObject.GetType() == typeof(AnimationClip))
        {
            var path = AssetDatabase.GetAssetPath(UnityEditor.Selection.activeObject);
            var split = path.Split('/');
            //Assets/GeniousFighterSubZero/ExportedClips/SubzeroThrow.fbx
            for(int i = split.Length-1; i > 0; i--)
            {
                string thisPath = "";
                for(int j = 0; j < i; j++)
                {
                    thisPath += split[j] + "/";
                }

                thisPath += FighterCreator.MANUALLY_EDITED_CLIPS_PATH;
                //Debug.Log("This Path: " + thisPath);
                if (AssetDatabase.LoadAssetAtPath<Object>(thisPath) != null)
                {
                    if (!path.StartsWith(thisPath))
                    {
                        pathToCopy = "";
                        for(int j = 0; j < split.Length-1; j++)
                        {
                            pathToCopy += split[j] + "/";
                        }

                        copyTargetLocation = thisPath;
                        animationClipName = UnityEditor.Selection.activeObject.name;
                        return true;
                    }
                    return false; //true;
                }
            }
        }

        return false;
    }

    DefaultAsset outputFolder;
    string newClipName;
    List<AnimationClip> clipsToCombine = new List<AnimationClip>(new AnimationClip[2]);
    AnimationClip combinedClip;
    void DisplayCombine()
    {
        EditorGUI.indentLevel++;
        outputFolder = EditorGUILayout.ObjectField("Output Path", outputFolder, typeof(DefaultAsset)) as DefaultAsset;
        newClipName = EditorGUILayout.TextField("Clip Name", newClipName);
        for (int i = 0; i < clipsToCombine.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            clipsToCombine[i] = EditorGUILayout.ObjectField("Clip " + (i+1), clipsToCombine[i], typeof(AnimationClip)) as AnimationClip;
            if (GUILayout.Button("X"))
            {
                clipsToCombine.RemoveAt(i--);
            }
            EditorGUILayout.EndHorizontal();
        }
        if (GUILayout.Button("Add Clip"))
        {
            clipsToCombine.Add(null);
        }

        if (GUILayout.Button("Combine"))
        {
            bool shouldCombine = true;
            foreach(var c in clipsToCombine)
            {
                if (c == null)
                {
                    shouldCombine = true;
                    Debug.LogError("Cannot combine: a requested clip is null");
                    break;
                }
            }

            if (outputFolder == null)
            {
                shouldCombine = false;
                Debug.LogError("Cannot combine: output folder is not assigned");
            }

            if (string.IsNullOrWhiteSpace(newClipName))
            {
                shouldCombine = false;
                Debug.Log("Cannot combine: invalid new file name");
            }

            if (shouldCombine)
            {
                combinedClip = Combine(outputFolder, newClipName, clipsToCombine.ToArray());
            }
        }

        if (combinedClip != null)
        {
            combinedClip = EditorGUILayout.ObjectField("Combined Clip", combinedClip, typeof(AnimationClip)) as AnimationClip;
        }
        EditorGUI.indentLevel--;
    }

    AnimationClip clipToSplit;
    List<int> framesToSplitOn;
    void DisplaySplit()
    {
        EditorGUI.indentLevel++;
        clipToSplit = EditorGUILayout.ObjectField("Clip to Split", clipToSplit, typeof(AnimationClip)) as AnimationClip;
        for (int i = 0; i < framesToSplitOn.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            clipsToCombine[i] = EditorGUILayout.ObjectField("Clip " + (i + 1), clipsToCombine[i], typeof(AnimationClip)) as AnimationClip;
            if (GUILayout.Button("X"))
            {
                clipsToCombine.RemoveAt(i--);
            }
            EditorGUILayout.EndHorizontal();
        }
        if (GUILayout.Button("Add Clip"))
        {
            clipsToCombine.Add(null);
        }
        EditorGUI.indentLevel--;
    }

    bool shouldDisplayCombine;
    bool shouldDisplaySplit;
    public void OnGUI()
    {
        shouldDisplayCombine = EditorGUILayout.Foldout(shouldDisplayCombine, "Combine");
        if (shouldDisplayCombine)
        {
            DisplayCombine();
        }

        shouldDisplaySplit = (EditorGUILayout.Foldout(shouldDisplaySplit, "Split"));
        if (shouldDisplaySplit)
        {
            DisplaySplit();
        }
    }

    AnimationClip Combine(DefaultAsset copyToFolder, string newClipName, params AnimationClip[] clips)
    {
        var first = clips[0];
        AnimationClip second;
        newClipName += ".anim";
        bool cleanup = false;
        if (clips.Length > 2)
        {
            var smaller = clips.ToList();
            smaller.RemoveAt(0);
            second = Combine(copyToFolder, "Temp_" + newClipName, smaller.ToArray());
            cleanup = true;
        }
        else
        {
            second = clips[1];
        }

        var ret = Combine(first, second, copyToFolder, newClipName);

        if (cleanup)
        {
            AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(second));
            AssetDatabase.Refresh();
        }
        return ret;
    }
    AnimationClip Combine(AnimationClip clip1, AnimationClip clip2, DefaultAsset copyToFolder, string newClipName)
    {
        return Combine(clip1, clip2, AssetDatabase.GetAssetPath(copyToFolder) + "/" + newClipName);
    }

    AnimationClip Combine(AnimationClip clip1, AnimationClip clip2, string newClipPath)
    {
        AnimationClip newClip;
        if (newClipPath == AssetDatabase.GetAssetPath(clip1))
        {
            newClip = AssetDatabase.LoadAssetAtPath<AnimationClip>(newClipPath);
        }
        else
        {
            newClip = SmashCharacterAnimatorTool.CopyClip(clip1, newClipPath);
        }
        if (newClip != null)
        {
            int countingIterations = 1;
            int bindingsLength = AnimationUtility.GetCurveBindings(newClip).Length;

            foreach (var b in AnimationUtility.GetCurveBindings(clip2))
            {
                AnimationCurve newCurve = new AnimationCurve();
                foreach (var k in AnimationUtility.GetEditorCurve(clip2, b).keys)
                {
                    newCurve.AddKey(k.time + clip1.length + (1f/clip1.frameRate), k.value);
                }

                foreach (var binding in AnimationUtility.GetCurveBindings(newClip))
                {
                    EditorUtility.DisplayProgressBar("[" + clip1.name + "] + [" + clip2.name + "] = " + newClip.name, binding.path, ((float)countingIterations++ / (float)bindingsLength));
                    if (binding.path == b.path)
                    {
                        AnimationCurve curve = AnimationUtility.GetEditorCurve(newClip, binding);
                        foreach (var k in curve.keys)
                        {
                            newCurve.AddKey(k.time, k.value);
                        }

                        AnimationUtility.SetEditorCurve(newClip, binding, null);
                        break;
                    }
                }

                EditorCurveBinding newBinding = EditorCurveBinding.FloatCurve(b.path, b.type, b.propertyName);
                AnimationUtility.SetEditorCurve(newClip, newBinding, newCurve);
            }

            EditorUtility.ClearProgressBar();
            return newClip;
        }

        EditorUtility.ClearProgressBar();
        return null;
    }

    void Split(AnimationClip clip, int frameToSplitOn, string clip1Path, string clip2Path)
    {

    }

    void CreatePingPong(AnimationClip clip, string outputPath)
    {

    }
}
