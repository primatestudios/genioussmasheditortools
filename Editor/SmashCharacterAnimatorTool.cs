using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

public class SmashCharacterAnimatorTool : EditorWindow
{
    [MenuItem("Genious/Fighter Animator Helper")]
    public static void ShowWindow()
    {
        GetWindow<SmashCharacterAnimatorTool>(false, "Animator Helper", true);
    }

    static FighterData selectedFighterData;
    [UnityEditor.MenuItem("Assets/Create\\Update Runtime Controller")]
    private static void UpdateRuntimeControllerAssetMenu()
    {
        if (SmashCharacterAnimatorTool.animatorTemplateController == null)
        {
            SmashCharacterAnimatorTool.InitDefaults();
        }

        if (SmashCharacterAnimatorTool.animatorTemplateController == null)
        {
            Debug.LogError("Cannot resolve Animator Template Controller automatically. Try using the Fighter Animator Helper window");
        }
        else if (selectedFighterData.setupAnimatorController == null)
        {
            Debug.LogError("No SetupAnimatorController is defined in the FighterData object for [" + selectedFighterData.characterName + "]. Aborting.");
        }
        else
        {
            if (EditorUtility.DisplayDialog("Create/Update RuntimeController",
                    "Are you sure you'd like to create/update runtime controller for character [" 
                    + selectedFighterData.characterName 
                    + "]? This process will use the setup controller [" 
                    + selectedFighterData.setupAnimatorController.name + "] and the template [" 
                    + SmashCharacterAnimatorTool.animatorTemplateController.name + "] as input.", 
                    "Update", "Cancel")
                )
            {
                var ogFighterData = SmashCharacterAnimatorTool.fighterData;
                var ogSetupController = SmashCharacterAnimatorTool.setupController;
                SmashCharacterAnimatorTool.fighterData = selectedFighterData;
                SmashCharacterAnimatorTool.setupController = fighterData.setupAnimatorController as AnimatorController;
                //TODO - pass in these jawns as arguments
                SmashCharacterAnimatorTool.CreateRuntimeController();
                SmashCharacterAnimatorTool.fighterData = ogFighterData;
                SmashCharacterAnimatorTool.setupController = ogSetupController;
            }
        }
    }

    [UnityEditor.MenuItem("Assets/Create\\Update Runtime Controller", true)]
    private static bool UpdateRuntimeControllerValidation()
    {
        selectedFighterData = null;
        if (UnityEditor.Selection.activeObject != null && UnityEditor.Selection.activeObject.GetType() == typeof(AnimatorController))
        {
            var fds = Resources.LoadAll<FighterData>("");
            foreach (var fd in fds)
            {
                if (fd.setupAnimatorController == UnityEditor.Selection.activeObject
                    || fd.runtimeAnimatorController == UnityEditor.Selection.activeObject)
                {
                    selectedFighterData = fd;
                    return true;
                }
            }
        }

        return false;
    }

    bool adjustRotationOfTransform = true;
    bool removeLastKeyIfSameAsFirst = true;
    public static FighterData fighterData;
    DefaultAsset animationPlacementFolder;
    UnityEditor.Animations.AnimatorController animatorToClone;
    static UnityEditor.Animations.AnimatorController animatorTemplateController;
    public static AnimatorController setupController {
        get
        {
            return fighterData == null ? null : fighterData.setupAnimatorController as AnimatorController;
        } set
        {
            if (fighterData != null)
            {
                fighterData.setupAnimatorController = value;
            }
        }
    }
    List<UpdateableAnimatorState> unsyncedStates;
    bool restartFromScratch;
    Vector2 scrollPosition;
    bool wantToClone;
    bool wantToUpdateMultipleRuntimeControllers;
    bool wantToUpdateTransitions;
    bool wantToUpdateRootMotion;
    bool wantToApplyMovesToStates;
    bool wantToCreateRuntimeController;
    bool wantToUpdateSetupController = true;
    DefaultAsset updatedAnimationsFolder;
    bool reupdateConvertedClips = false;
    AnimatorController transitionAnimatorController;
    AnimatorController toFixPrefix;
     
    bool triedToGetTemplate;
    bool triedToGetFrameDataFolder;

    [UnityEditor.Callbacks.DidReloadScripts]
    public static void InitDefaults()
    {
        animatorTemplateController = AssetDatabase.LoadAssetAtPath<AnimatorController>("Assets/GeniousArena/Art/Animation/Controller/Character Animation Controller Template.controller");
    }

    public void Awake()
    {
        InitDefaults();
    }

    bool? requestSaveProjectOnDisplayRuntimeController;
    public void OnGUI()
    {
        try
        {
            EditorGUIUtility.labelWidth = 250;
            bool wasNull = fighterData == null;
            fighterData = EditorGUILayout.ObjectField("Character Frame Data", fighterData, typeof(FighterData)) as FighterData;
            if (fighterData != null)
            {
                if (!wasNull)
                {
                    transformRootName = fighterData.boneRoot;
                }
                wantToClone = EditorGUILayout.Foldout(wantToClone, "Clone Setup Controller");
                if (wantToClone)
                {
                    DisplayCloneSettings();
                }

                wantToUpdateSetupController = EditorGUILayout.Foldout(wantToUpdateSetupController, "Update Setup Controller");
                if (wantToUpdateSetupController)
                {
                    DisplayUpdateSetupControllerSettings();
                }

                wantToUpdateRootMotion = EditorGUILayout.Foldout(wantToUpdateRootMotion, "Fix Root Motion");
                if (wantToUpdateRootMotion)
                {
                    DisplayRootMotionSettings();
                }

                wasDisplayingRootMotion = wantToUpdateRootMotion;

                wantToApplyMovesToStates = EditorGUILayout.Foldout(wantToApplyMovesToStates, "Apply Moves To States");
                if (wantToApplyMovesToStates)
                {
                    DisplyApplyMoveToStateSettings();
                }

                wantToCreateRuntimeController = EditorGUILayout.Foldout(wantToCreateRuntimeController, "Create/Update Runtime Controller");
                if (wantToCreateRuntimeController)
                {
                    if (requestSaveProjectOnDisplayRuntimeController == null)
                    {
                        requestSaveProjectOnDisplayRuntimeController = true;
                    } 
                    DisplayRuntimeControllerSettings();
                }
                else
                {
                    requestSaveProjectOnDisplayRuntimeController = null;
                }
            }

            wantToUpdateTransitions = EditorGUILayout.Foldout(wantToUpdateTransitions, "Update Transitions");

            if (wantToUpdateTransitions)
            {
                DisplayTransitionSettings();
            }

            wantToUpdateMultipleRuntimeControllers = EditorGUILayout.Foldout(wantToUpdateMultipleRuntimeControllers, "Update Multiple Runtime Controllers");
            if (wantToUpdateMultipleRuntimeControllers)
            {
                DisplayUpdateManyRuntimeControllerSettings();
            }

            /*
            toFixPrefix = EditorGUILayout.ObjectField("To Fix", toFixPrefix, typeof(AnimatorController), false) as AnimatorController ;
            if (GUIButton("Fix Motions with incorrect parents"))
            {
                FixUnparentedMotions(true);
            }

            if (GUIButton("Fix 2"))
            {
                FixUnparentedMotions(false);
            }
            */
        }
        catch (Exception e)
        {
            if (e.GetType() != typeof(ExitGUIException))
            {
                Debug.LogError("Exception: " + e);
                EditorUtility.ClearProgressBar();
            }
        }
    }

    void FixUnparentedMotions(bool method)
    {
        foreach (var a in GetAnimatorStates(toFixPrefix))
        {
            var m = a.motion;
            if (m is AnimationClip)
            {
                var clip = m as AnimationClip;
                List<EditorCurveBinding> bindings = new List<EditorCurveBinding>();
                float lastKeyFrame = Mathf.NegativeInfinity;

                foreach (var binding in AnimationUtility.GetCurveBindings(clip))
                {
                    if (method && !String.IsNullOrWhiteSpace(binding.path))// == "TransN" && binding.propertyName.StartsWith("m_LocalPosition"))
                    {
                        Debug.Log("Curve Binding: " + binding.path);
                        var newBindingPath = "Model/" + binding.path.Replace("Model/", "");
                        Debug.Log("New binding: " + newBindingPath + " - " + binding.type +  " and " + binding.propertyName);
                        AnimationCurve curve = AnimationUtility.GetEditorCurve(clip, binding);
                        EditorCurveBinding newBinding = EditorCurveBinding.FloatCurve(newBindingPath, binding.type, binding.propertyName);
                        AnimationUtility.SetEditorCurve(clip, newBinding, curve);
                        AnimationUtility.SetEditorCurve(clip, binding, null);
                    }
                    else if (!method && String.IsNullOrWhiteSpace(binding.path))
                    {
                        AnimationCurve curve = AnimationUtility.GetEditorCurve(clip, binding);
                        EditorCurveBinding newBinding = EditorCurveBinding.FloatCurve("", typeof(FighterModel), binding.propertyName);
                        AnimationUtility.SetEditorCurve(clip, newBinding, curve);
                        AnimationUtility.SetEditorCurve(clip, binding, null);
                    }
                }

                continue;

                foreach (var binding in bindings)
                {
                    AnimationCurve curve = AnimationUtility.GetEditorCurve(clip, binding);

                    string suffix = binding.propertyName.Substring(binding.propertyName.Length - 1);
                    if (adjustRotationOfTransform)
                    {
                        if (suffix == "z")
                        {
                            suffix = "y";
                            for (int i = 0; i < curve.keys.Length; i++)
                            {
                                Keyframe newKeyframe = curve.keys[i];
                                newKeyframe.value = -newKeyframe.value;
                                curve.MoveKey(i, newKeyframe);
                            }
                        }
                        else if (suffix == "y")
                        {
                            suffix = "z";
                        }
                    }
                 
                    AnimationUtility.SetEditorCurve(clip, binding, null);
                    //                clip.AddEvent()
                }

                AnimationClipCurveData[] curveDatas = AnimationUtility.GetAllCurves(clip, true);
                List<string> targetObjectPaths = new List<string>();
                foreach (var curveData in curveDatas)
                {
                    while (curveData.path.StartsWith("Model/"))
                    {
                        curveData.path = curveData.path.Replace("Model/", "");
                    }
                    Debug.Log(m.name + " path before: " + curveData.path);
                    if (!string.IsNullOrEmpty(curveData.path))
                    {
                        curveData.path = "Model/" + curveData.path;
                    }
                    Debug.Log(m.name + " path After: " + curveData.path);

                }

                clip.ClearCurves();
                foreach (var curveData in curveDatas)
                {
                    clip.SetCurve(curveData.path, curveData.type, curveData.propertyName, curveData.curve);
                }
                continue;

                //var bindings = new List<EditorCurveBinding>();]
             

                foreach (var binding in bindings)
                {
                    AnimationCurve curve = AnimationUtility.GetEditorCurve(clip, binding);
                    //Debug.Log(curve.)
                    continue;
                    //lastKeyFrame = Mathf.Max(lastKeyFrame, curve.keys[curve.keys.Length - 1].time);

                    if (removeLastKeyIfSameAsFirst)
                    {
                        if (curve.keys[0].value == curve.keys[curve.keys.Length - 1].value)
                        {
                            curve.RemoveKey(curve.keys.Length - 1);
                        }
                    }

                    string suffix = binding.propertyName.Substring(binding.propertyName.Length - 1);
                    if (adjustRotationOfTransform)
                    {
                        if (suffix == "z")
                        {
                            suffix = "y";
                            for (int i = 0; i < curve.keys.Length; i++)
                            {
                                Keyframe newKeyframe = curve.keys[i];
                                newKeyframe.value = -newKeyframe.value;
                                curve.MoveKey(i, newKeyframe);
                            }
                        }
                        else if (suffix == "y")
                        {
                            suffix = "z";
                        }
                    }
                    EditorCurveBinding newBinding = EditorCurveBinding.FloatCurve("", typeof(FighterController), "animationMovementOffset." + suffix);
                    AnimationUtility.SetEditorCurve(clip, newBinding, curve);
                    AnimationUtility.SetEditorCurve(clip, binding, null);
                    //                clip.AddEvent()
                }
            }
        }
    }

    Dictionary<FighterData, KeyValuePair<bool, AnimatorController>> frameDataToUpdate = new Dictionary<FighterData, KeyValuePair<bool, AnimatorController>>();//<FighterData>();
    FighterData[] fighters;
    void DisplayUpdateManyRuntimeControllerSettings()
    {
        EditorGUI.indentLevel++;
        animatorTemplateController = EditorGUILayout.ObjectField("Animator Template", animatorTemplateController, typeof(UnityEditor.Animations.AnimatorController)) as UnityEditor.Animations.AnimatorController;
        //updateStateMachineMoveBehaviours = EditorGUILayout.Toggle("Update State Machine Moves", updateStateMachineMoveBehaviours);
        if (frameDataToUpdate == null || frameDataToUpdate.Count <= 0)
        {
            fighters = Resources.LoadAll<FighterData>("");
            foreach(var f in fighters)
            {
                frameDataToUpdate[f] = new KeyValuePair<bool, AnimatorController>(true, null);
            }
        }
       /* if (x != frameDataFolder || (frameDataFolder != null && frameDataToUpdate.Count == 0 && GUILayout.Button("Refresh")))
        {
            frameDataFolder = x;
            frameDataToUpdate.Clear();
            if (x != null)
            {
                foreach (var guid in AssetDatabase.FindAssets("t:FighterData", new string[] { AssetDatabase.GetAssetPath(frameDataFolder) }))
                {
                    var path = AssetDatabase.GUIDToAssetPath(guid);
                    frameDataToUpdate[AssetDatabase.LoadAssetAtPath<FighterData>(path)] = new KeyValuePair<bool, AnimatorController>(true, null);
                }
            }
        }
        */
        bool showButton = false;// frameDataToUpdate.Count > 0;

        EditorGUI.indentLevel++;
        if (frameDataToUpdate != null && frameDataToUpdate.Count > 0 && GUILayout.Button("Select/Deselect All"))
        {
            bool allTrue = true;
            foreach(var pair in frameDataToUpdate)
            {
                if (pair.Key.setupAnimatorController != null)
                {
                    if (!pair.Value.Key)
                    {
                        allTrue = false;
                        break;
                    }
                }
            }

            foreach (var key in frameDataToUpdate.Keys.ToArray())
            {
                frameDataToUpdate[key] = new KeyValuePair<bool, AnimatorController>(!allTrue, null);// frameDataToUpdate[key].Value);
            }
        }
        foreach (var key in frameDataToUpdate.Keys.ToList())
        {
            if (key.setupAnimatorController != null)
            {
                var val = frameDataToUpdate[key];
                if (frameDataToUpdate[key].Value == null)
                {
                    frameDataToUpdate[key] = new KeyValuePair<bool, AnimatorController>( EditorGUILayout.Toggle(key.displayName, val.Key), null);
                    showButton |= frameDataToUpdate[key].Key;
                }
                else
                {
                    var field = EditorGUILayout.ObjectField(key.displayName, val.Value, typeof(UnityEditor.Animations.AnimatorController));
                    if (field == null)
                    {
                        frameDataToUpdate[key] = new KeyValuePair<bool, AnimatorController>(frameDataToUpdate[key].Key, null);
                    }
                }
            }
            else
            {
                frameDataToUpdate[key] = new KeyValuePair<bool, AnimatorController>( false, null);
            }
        }
        EditorGUI.indentLevel--;

        if (showButton && GUILayout.Button("Update Runtime Animation Controllers"))
        {
            var ogFrameData = fighterData;
            AnimatorController ogSetupController = setupController;
            foreach (var thisFrameData in frameDataToUpdate.Keys.ToList())
            {
                if (frameDataToUpdate[thisFrameData].Key)
                {
                    fighterData = thisFrameData;
                    setupController = fighterData.setupAnimatorController as AnimatorController;
                    //TODO - pass in these jawns as arguments
                    CreateRuntimeController();
                    frameDataToUpdate[thisFrameData] = new KeyValuePair<bool, AnimatorController>(true, thisFrameData.runtimeAnimatorController as AnimatorController);
                }
            }

            fighterData = ogFrameData;
            setupController = ogSetupController;
            RequestSaveProject();
        }

        EditorGUI.indentLevel--;
    }

    Dictionary<string, AnimatorControllerParameter> possibleParameters = new Dictionary<string, AnimatorControllerParameter>();
    List<BrokenTransition> brokenTransitions = new List<BrokenTransition>();
    void DisplayTransitionSettings()
    {
        EditorGUI.indentLevel++;
        var s = EditorGUILayout.ObjectField("Animator", transitionAnimatorController, typeof(UnityEditor.Animations.AnimatorController)) as UnityEditor.Animations.AnimatorController;
        if (transitionAnimatorController != null && GUILayout.Button("Update Parameters"))
        {
            UpdateInputParameters(transitionAnimatorController);
            return;
        }

            if (s != null && (s != transitionAnimatorController || GUILayout.Button("Refresh")))
        {
            transitionAnimatorController = s;
            UpdateAnimatorParameters(s);
            UpdateBrokenTransitions();
        }

        transitionAnimatorController = s;
        DisplayBrokenTransitions();
        EditorGUI.indentLevel--;
    }


    void DisplayBrokenTransitions()
    {
        if (brokenTransitions == null)
        {
            return;
        }

        if (brokenTransitions != null && brokenTransitions.Count > 0 && GUILayout.Button("Save Parameters"))
        {
            foreach(var bt in brokenTransitions)
            {
                if (bt.newTransitionParameter != null)
                {
                   // var parameterValue = possibleParameters[bt.newTransitionParameter];
                    bt.brokenTransition.AddCondition(bt.brokenCondition.mode, bt.brokenCondition.threshold, bt.newTransitionParameter);
                    bt.brokenTransition.RemoveCondition(bt.brokenCondition);
                }
            }

            UpdateBrokenTransitions();
        }

        var oldWordWrap = EditorStyles.label.wordWrap;
        EditorStyles.label.wordWrap = true;
        EditorGUILayout.LabelField("NOTE - These parameters will only save if they are of the proper type. Use the Mode and Threshold to determine what type of parameter is needed.");
        EditorStyles.label.wordWrap = oldWordWrap;
        List<String> possibleValues = new List<string>(new string[] { "[Missing]" });
        possibleValues.AddRange(possibleParameters.Keys.OrderBy(x => x));
        var arr = possibleValues.ToArray();
        foreach(var bt in brokenTransitions)
        {
            foreach(var condition in bt.brokenTransition.conditions)
            {
                EditorGUILayout.BeginHorizontal();
                if (!possibleValues.Contains(condition.parameter))
                {
                    EditorGUILayout.ObjectField(condition.parameter, bt.reference, typeof(AnimatorState));
                    EditorGUILayout.LabelField("Missing Parameter: " + condition.parameter + " | Mode: " + condition.mode + " | Threshold: " + condition.threshold );
                    var s = EditorGUILayout.Popup(bt.GetTransitionIndex(condition.parameter), arr);
                    if (s == 0)
                    {
                        bt.newTransitionParameter = null;
                    }
                    else
                    {
                        bt.SetTransitionIndex(condition.parameter, s);
                        bt.newTransitionParameter = arr[s];
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
        }
    }

    void UpdateBrokenTransitions()
    {
        brokenTransitions.Clear();
        
        if (transitionAnimatorController == null)
        {
            return;
        }

        possibleParameters = new Dictionary<string, AnimatorControllerParameter>();
        foreach (var p in transitionAnimatorController.parameters)
        {
            possibleParameters[p.name] =  p;
        }
        Dictionary<AnimatorTransitionBase, UnityEngine.Object> transitions = new Dictionary<AnimatorTransitionBase, UnityEngine.Object>();
        HashSet<AnimatorTransitionBase> anyStateTransitions = new HashSet<AnimatorTransitionBase>();
        foreach (var state in GetAnimatorStates(transitionAnimatorController))
        {
            foreach(var t in state.transitions)
            {
                transitions[t] = state;
            }
        }

        foreach(var sm in GetAnimatorStateMachines(transitionAnimatorController))
        {
            var transitionsToCheck = sm.entryTransitions;
            /*foreach(var st in sm.entryTransitions)
            {
                //Debug.Log("Entry Transitions of " + sm.name);
                foreach(var c in st.conditions)
                {
                    Debug.Log(c.parameter);
                }
            }*/
            foreach(var sm2 in GetAnimatorStateMachines(transitionAnimatorController))
            {
               // Debug.Log("Number of Transitions between " + sm.name + " and " + sm2.name + " -> " + sm.GetStateMachineTransitions(sm2).Length);
                transitionsToCheck.Concat(sm.GetStateMachineTransitions(sm2));
            }
            foreach(var t in transitionsToCheck)
            {
                transitions[t] = sm;
            }

            foreach (var t in sm.anyStateTransitions)
            {
                transitions[t] = sm;
                anyStateTransitions.Add(t);
            }
        }

        foreach (var pair in transitions)
        {
            var t = pair.Key;
            foreach (var c in t.conditions)
            {
               // Debug.Log("Condition: " + c.parameter);
                bool parameterExists = false;
                foreach (var p in transitionAnimatorController.parameters)
                {
                    if (p.name == c.parameter)
                    {
                        parameterExists = true;
                        break;
                    }
                }

                if (!parameterExists)
                {
                    //Debug.Log("Missing Parameter: " + c.parameter);
                    brokenTransitions.Add(new BrokenTransition(pair.Value, t, c));
                }
            }
        }
    }

    void DisplayCloneSettings()
    {
        EditorGUI.indentLevel++;
        animatorToClone = EditorGUILayout.ObjectField("Animator [Setup] To Clone", animatorToClone, typeof(UnityEditor.Animations.AnimatorController)) as UnityEditor.Animations.AnimatorController;
        string prefix = fighterData != null ? fighterData.displayName + " " : "";
        animationPlacementFolder = EditorGUILayout.ObjectField(prefix + "Animation Import Folder", animationPlacementFolder, typeof(DefaultAsset)) as DefaultAsset;
        restartFromScratch = EditorGUILayout.Toggle("Restart From Scratch", restartFromScratch);
        if (GUILayout.Button("Clone Setup Controller"))
        {
            setupController = Clone();
            referenceController = animatorToClone;
            UpdateUnsyncedStates();
        }
        EditorGUI.indentLevel--;
    }

    bool wasDisplayingRootMotion;
    string transformRootName = "TransN";
    void DisplayRootMotionSettings()
    {
        EditorGUI.indentLevel++;

        if (!wasDisplayingRootMotion && updatedAnimationsFolder == null && fighterData != null)
        {

        }
        setupController = EditorGUILayout.ObjectField("Animator [Setup] Controller", setupController, typeof(UnityEditor.Animations.AnimatorController)) as UnityEditor.Animations.AnimatorController;
        updatedAnimationsFolder = EditorGUILayout.ObjectField("Updated Animation Folder", updatedAnimationsFolder, typeof(DefaultAsset)) as DefaultAsset;
        reupdateConvertedClips = EditorGUILayout.Toggle("Convert clips that have already been converted", reupdateConvertedClips); ;
        transformRootName = EditorGUILayout.TextField("Transform Root Name", transformRootName);

        if (GUILayout.Button("Create Root Motion Clips"))
        {
            UpdateRootMotion();
        }

        EditorGUI.indentLevel--;
    }

    void DisplayUpdateSetupControllerSettings()
    {
        EditorGUI.indentLevel++;
        var newSetup = EditorGUILayout.ObjectField("Animator [Setup] Controller", setupController, typeof(UnityEditor.Animations.AnimatorController), false) as UnityEditor.Animations.AnimatorController;
        var c = EditorGUILayout.ObjectField("Reference Controller [Setup]", referenceController, typeof(UnityEditor.Animations.AnimatorController), false) as UnityEditor.Animations.AnimatorController;

        if (GUILayout.Button("Refresh Unsynced States") || newSetup != setupController || c != referenceController)
        {
            referenceController = c;
            setupController = newSetup;
            if (setupController != null)
            {
                Debug.Log("Updating!");
                UpdateInputParameters(setupController);
                UpdateUnsyncedStates();
           }
        }
        DisplayUnsyncedStates();
        EditorGUI.indentLevel--;
    }

    void Combine(AnimationClip clip1, AnimationClip clip2, DefaultAsset copyToFolder, string newClipName)
    {
        Combine(clip1, clip2, AssetDatabase.GetAssetPath(copyToFolder) + "/" + newClipName);
    }

    void Combine(AnimationClip clip1, AnimationClip clip2, string newClipPath)
    {
        if (AssetDatabase.CopyAsset(AssetDatabase.GetAssetPath(clip1), newClipPath))
        {
            var newClip = AssetDatabase.LoadAssetAtPath<AnimationClip>(newClipPath);
            HashSet<EditorCurveBinding> curveBindingsToHandle = new HashSet<EditorCurveBinding>(AnimationUtility.GetCurveBindings(clip2));

            foreach (var binding in AnimationUtility.GetCurveBindings(newClip))
            {
                foreach(var b in curveBindingsToHandle.ToArray())
                {
                    if (binding.path == b.path)
                    {
                        AnimationCurve curve = AnimationUtility.GetEditorCurve(newClip, binding);
                        var clip2Curve = AnimationUtility.GetEditorCurve(clip2, b);
                        curve.keys = curve.keys.Concat(clip2Curve.keys).ToArray();
                        curveBindingsToHandle.Remove(b);
                        AnimationUtility.SetEditorCurve(newClip, binding, curve);
                        break;
                    }
                }
            }

            foreach(var b in curveBindingsToHandle)
            {
                AnimationUtility.SetEditorCurve(newClip, b, AnimationUtility.GetEditorCurve(clip2, b));
            }
        }
    }

    void Split(AnimationClip clip, int frameToSplitOn, string clip1Path, string clip2Path)
    {

    }

    //  bool updateStateMachineMoveBehaviours;

    void DisplayRuntimeControllerSettings()
    {
        EditorGUI.indentLevel++;
        setupController = EditorGUILayout.ObjectField("Animator [Setup] Controller", setupController, typeof(UnityEditor.Animations.AnimatorController), GUILayout.ExpandWidth(true)) as UnityEditor.Animations.AnimatorController;
        animatorTemplateController = EditorGUILayout.ObjectField("Animator Template", animatorTemplateController, typeof(UnityEditor.Animations.AnimatorController)) as UnityEditor.Animations.AnimatorController;
        //updateStateMachineMoveBehaviours = EditorGUILayout.Toggle("Update State Machine Moves", updateStateMachineMoveBehaviours);
        if (GUILayout.Button("Create Runtime Controller"))
        {
            CreateRuntimeController();
            RequestSaveProject();
        }
        DisplayRuntimeControllerResults();
        if (requestSaveProjectOnDisplayRuntimeController == true)
        {
            RequestSaveProject();
            requestSaveProjectOnDisplayRuntimeController = false;
        }
        EditorGUI.indentLevel--;
    }
      
    void DisplyApplyMoveToStateSettings()
    {
        EditorGUI.indentLevel++;
        setupController = EditorGUILayout.ObjectField("Animator [Setup] Controller", setupController, typeof(UnityEditor.Animations.AnimatorController), GUILayout.ExpandWidth(true)) as UnityEditor.Animations.AnimatorController;
        if (GUILayout.Button("Update"))
        {
            ApplyStateMachineMoveBehaviours(MatchAnimatorStatesAndFighterData(setupController));
            EditorUtility.ClearProgressBar();
        }
        EditorGUI.indentLevel--;
    }

    void DisplayRuntimeControllerResults()
    {
        EditorGUI.indentLevel++;
        if (runtimeResults != null)
        {
            EditorGUILayout.ObjectField(fighterData.displayName + " Runtime Controller", runtimeResults.runtimeController, typeof(UnityEditor.Animations.AnimatorController));

            runtimeResults.displayUnmatchedStates = EditorGUILayout.Foldout(runtimeResults.displayUnmatchedStates, "Unhandled States");
            if (runtimeResults.displayUnmatchedStates)
            {
                foreach (var s in runtimeResults.unmatchedStates)
                {
                    bool probablyASmashAction = true;
                    foreach (var b in s.behaviours)
                    {
                        if (b is NotAFighterAction)
                        {
                            probablyASmashAction = false;
                            break;
                        }
                    }
                    if (probablyASmashAction)
                    {
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.ObjectField(s.name, s, typeof(AnimatorState));
                        if (GUILayout.Button("Not An Action"))
                        {
                            if (setupController != null)
                            {
                                foreach (var s2 in GetAnimatorStates(setupController).Concat(GetAnimatorStates(animatorTemplateController)))
                                {
                                    if (s2.name == s.name)
                                    {
                                        bool alreadyAdded = false;
                                        foreach (var b in s2.behaviours)
                                        {
                                            if (b is NotAFighterAction)
                                            {
                                                alreadyAdded = true;
                                                break;
                                            }
                                        }

                                        if (!alreadyAdded)
                                        {
                                            s2.AddStateMachineBehaviour<NotAFighterAction>();
                                        }
                                    }
                                }
                            }

                            s.AddStateMachineBehaviour<NotAFighterAction>();
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
            }

            runtimeResults.displayUnmatchedActions = EditorGUILayout.Foldout(runtimeResults.displayUnmatchedActions, "Unhandled Moves");
            if (runtimeResults.displayUnmatchedActions)
            {
                EditorGUI.indentLevel++;
                foreach(var a in runtimeResults.unmatchedActions)
                {
                    EditorGUILayout.LabelField(a.actionName);
                }
                EditorGUI.indentLevel--;
            }
        }
        EditorGUI.indentLevel--;
    }

    static RuntimeAnimatorCreatorResults runtimeResults;
    static void CreateRuntimeController()
    {
        UpdateAnimatorParameters(animatorTemplateController);
        AnimatorController runtimeController = CreateAndPopulateRuntimeControllerAsset();
        runtimeResults = MatchAnimatorStatesAndFighterData(runtimeController);
        /*if (updateStateMachineMoveBehaviours)
        {
            ApplyStateMachineMoveBehaviours(runtimeResults);
        }*/
        UpdateStatesSpeed(runtimeResults);
        EditorUtility.ClearProgressBar();
    }

    static bool ShouldReplaceMotionOnly(AnimatorController thisSetupController, AnimatorStateMachine stateMachine)
    {
       foreach(var topLevelStateMachine in thisSetupController.layers[0].stateMachine.stateMachines)
        {
            if (topLevelStateMachine.stateMachine.name == "Motion Replacement Only")
            {
                foreach(var sm in GetAnimatorStateMachines(topLevelStateMachine.stateMachine))
                {
                    if (sm.name == stateMachine.name)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    static AnimatorController CreateAndPopulateRuntimeControllerAsset()
    {
        UpdateProgressBar("(" + fighterData.displayName + ") Creating & Populating Runtime Controller");

        var duplicate = AssetDatabase.GetAssetPath(setupController).Split('.')[0].Trim() + "[Runtime].controller";
        duplicate = duplicate.Replace("[Setup]", "");
        AssetDatabase.DeleteAsset(duplicate);
        var templatePath = AssetDatabase.GetAssetPath(animatorTemplateController);
        AssetDatabase.CopyAsset(AssetDatabase.GetAssetPath(animatorTemplateController), duplicate);
    
        AssetDatabase.Refresh();
        var newController = AssetDatabase.LoadAssetAtPath<AnimatorController>(duplicate);

        //var newControllerStateMachines = newController.layers[0].stateMachine;
        // var specificCharacterStateMachines = setupController.layers[0].stateMachine;

        /* Dictionary<string, HashSet<AnimatorStateMachine>> defaultStates = new Dictionary<string, HashSet<AnimatorStateMachine>>();
         foreach (var runtimeStateMachine in GetAnimatorStateMachines(newController))
         {
             if (runtimeStateMachine.defaultState == null)
             {
                 continue;
             }

             if (!defaultStates.ContainsKey(runtimeStateMachine.defaultState.name))
             {
                 defaultStates[runtimeStateMachine.defaultState.name] = new HashSet<AnimatorStateMachine>();
             }

             defaultStates[runtimeStateMachine.defaultState.name].Add(runtimeStateMachine);
         }*/

        var runtimeStateMachines = GetAnimatorStateMachines(newController);
        for (int i = 0; i < runtimeStateMachines.Count; i++)
        {
            var runtimeStateMachine = runtimeStateMachines[i];
            foreach (var setupStateMachine in GetAnimatorStateMachines(setupController))
            {
                if (runtimeStateMachine.name == setupStateMachine.name)
                {
                    string defaultStateName = null;
                    var runtimeStates = GetAnimatorStates(runtimeStateMachine);
                    var setupStates = GetChildAnimatorStates(setupStateMachine);
                    var firstLevelSetupStateMachines = setupStateMachine.stateMachines;
                    var firstLevelSetupStates = setupStateMachine.states;
                    if (ShouldReplaceMotionOnly(setupController, setupStateMachine))
                    {
                        if (runtimeStates.Count > 0)
                        {
                            foreach (var runtimeState in runtimeStates)
                            {
                                foreach (var setupState in setupStates)//.states)
                                {
                                    if (setupState.state.name == runtimeState.name)
                                    {
                                        runtimeState.motion = setupState.state.motion;
                                        runtimeState.speed = setupState.state.speed;

                                      //  Debug.Log(setupState.state.name + " - " + setupState.state.speed + "(" + runtimeState.speed + ")"); ;  

                                        //If the setup controller has a behaviour, use that one (instead of the one in the template)
                                        //  
                                        var setupBehavioursHashSet = new HashSet<StateMachineBehaviour>(setupState.state.behaviours);
                                        List<StateMachineBehaviour> newRuntimeBehaviours = new List<StateMachineBehaviour>();
                                        foreach (var runtimeBehaviour in runtimeState.behaviours)
                                        {
                                            bool runtimeBehaviourReplaced = false;
                                            foreach (var setupBehaviour in setupBehavioursHashSet.ToList())
                                            {
                                                if (setupBehaviour.GetType() == runtimeBehaviour.GetType())
                                                {
                                                    newRuntimeBehaviours.Add(setupBehaviour);
                                                    setupBehavioursHashSet.Remove(setupBehaviour);
                                                    runtimeBehaviourReplaced = true;
                                                    break;
                                                }
                                            }
                                            if (!runtimeBehaviourReplaced)
                                            {
                                                newRuntimeBehaviours.Add(runtimeBehaviour);
                                            }

                                            foreach(var setupBehaviourNotYetAdded in setupBehavioursHashSet)
                                            {
                                                newRuntimeBehaviours.Add(setupBehaviourNotYetAdded);
                                            }
                                        }
                                        runtimeState.behaviours = newRuntimeBehaviours.ToArray();// setupState.state.behaviours;
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Debug.LogError("Error with StateMachine '" + setupStateMachine.name + "' - Attempting to only replace motions, but no motions found in the runtime controller.");
                        }
                    }
                    else//  if (newStateMachine.states.Length == 0)
                    {
                        if (runtimeStates.Count > 0)
                        {
                            foreach (var runtimeState in runtimeStates)//.states)
                            {
                                if (runtimeState == runtimeStateMachine.defaultState)
                                {
                                    defaultStateName = runtimeState.name;
                                }
                                runtimeStateMachine.RemoveState(runtimeState);
                            }
                        }

                        //It's not easy (I can't figure out how) to copy over sub-state machines while keeping the links in place. Try to use a one-level system
                        foreach (var setupState in setupStates)
                         {
       
                            runtimeStateMachine.AddState(setupState.state, setupState.position);
                        }

                        runtimeStateMachine.defaultState = setupStateMachine.defaultState;
                        CopyStateMachinePositions(setupStateMachine, ref runtimeStateMachine);
                        //Debug.Log("Copied states to " + runtimeStateMachine.name);
                    }

                    /*
                    foreach (var runtimeState in GetAnimatorStates(runtimeStateMachine))
                    {
                        foreach(var defaultStatePair in defaultStates)
                        {
                            if (defaultStatePair.Key == runtimeState.name)
                            {
                                foreach(var sm in defaultStatePair.Value)
                                {
                                    sm.defaultState = runtimeState;
                                }
                                defaultStateName = null;
                                //Debug.Log("Set " + runtimeState.name + " as default state for " + defaultStatePair.Value.Count + " jawns");
                            }
                        }
                    }

                    Debug.Log("Default State: " + defaultStateName);

                    if (defaultStateName != null)
                    {
                        Debug.LogError("Failed to set Default State in " + setupStateMachine.name + " to " + defaultStateName);
                    }
                    */
                }
            }
            runtimeStateMachines[i] = runtimeStateMachine;
        }

        fighterData.runtimeAnimatorController = newController;
        EditorUtility.SetDirty(fighterData);
        AssetDatabase.Refresh();
        return newController;
    }

    static void CopyStateMachinePositions(AnimatorStateMachine sourceStateMachine, ref AnimatorStateMachine destinationStateMachine)
    {
        destinationStateMachine.anyStatePosition = sourceStateMachine.anyStatePosition;
        destinationStateMachine.parentStateMachinePosition = sourceStateMachine.parentStateMachinePosition;
        destinationStateMachine.exitPosition = sourceStateMachine.exitPosition;
        destinationStateMachine.entryPosition = sourceStateMachine.entryPosition;
    }

    static void UpdateStatesSpeed(RuntimeAnimatorCreatorResults parsedData)
    {
        foreach (var pair in parsedData.stateFrames)
        {
            var state = pair.Key;
            if (state.motion != null && pair.Value.numFrames > 0)
            {
                UpdateStateSpeed(pair.Key, pair.Value);
            }
        }

        foreach(var s in GetAnimatorStates(parsedData.runtimeController))
        {
            if (s.name.Contains("Smash Charge"))
            {
                UpdateStateSpeed(s, 60);
            }

            foreach(var b in s.behaviours)
            {
                if (b is Landing)
                {
                    Frame frameCount = (int) (((Landing)b).softLand ? fighterData.attributes.soft_landing_lag : fighterData.attributes.hard_landing_lag);
                    UpdateStateSpeed(s, frameCount);
                }
            }
        }
    }

    void ApplyStateMachineMoveBehaviours(RuntimeAnimatorCreatorResults parsedData)
    {
        UpdateProgressBar("Matching States with frame data");
        var matched = parsedData.matched;
        //This is only done so that things with similar names aren't matched multiple times (I.E "Dancing Blade" and "Dancing Blade (Up)")
        Dictionary<Move, List<AnimatorState>> possibleMatchedMoves = new Dictionary<Move, List<AnimatorState>>();
        foreach (var pair in matched)
        {
            foreach (var move in fighterData.moves)
            {
                if (move.name.Contains("Heli"))
                {
                    Debug.Log("Trying to match " + move.moveName + " with " + pair.Value.name);
                }
                if (move.moveName.ToLower().Trim().Contains(pair.Value.name.ToLower().Trim()))
                {
                    if (!possibleMatchedMoves.ContainsKey(move))
                    {
                        possibleMatchedMoves[move] = new List<AnimatorState>();
                    }
                    possibleMatchedMoves[move].Add(pair.Value);
                }
            }
        }

        Dictionary<AnimatorState, List<Move>> actualMatches = new Dictionary<AnimatorState, List<Move>>();
        foreach (var key in possibleMatchedMoves.Keys.ToArray())
        {
            AnimatorState biggestName = null;
            if (possibleMatchedMoves[key].Count == 1)
            {
                biggestName = possibleMatchedMoves[key][0];
            }
            else if (possibleMatchedMoves[key].Count > 1)
            {
                foreach (var s in possibleMatchedMoves[key])
                {
                    if (biggestName == null || s.name.Length > biggestName.name.Length)
                    {
                        biggestName = s;
                    }
                }
            }

            if (biggestName != null)
            {
                if (!actualMatches.ContainsKey(biggestName))
                {
                    actualMatches[biggestName] = new List<Move>();
                }
                actualMatches[biggestName].Add(key);
            }
        }

        foreach (var pair in matched)
        {
            ApplyMotionToState(pair.Value, pair.Key.clip);
            var list = pair.Value.behaviours.ToList();
            bool containsDontMove = false; ;
            bool containsDontRotate = false;
            bool containsNotAFighterAction = false;
            for (int i = 0; i < list.Count; i++)
            {
                var b = pair.Value.behaviours[i];
                if (b is NotAFighterAction)
                {
                    if (containsNotAFighterAction)
                    {
                        list.RemoveAt(i--);
                    }
                    else
                    {
                        containsNotAFighterAction = true;
                    }
                }
                if (b is FighterAnimatorAction)
                {
                    list.RemoveAt(i--);
                }
                else if (b is StopMovement)
                {
                    containsDontMove = true;
                }
                else if (b is StopRotation)
                {
                    containsDontRotate = true;
                }
            }

            pair.Value.behaviours = list.ToArray();

            if (!actualMatches.ContainsKey(pair.Value))
            {
                continue;
            }
            foreach (var move in actualMatches[pair.Value])
            {
                if (IsMatchException(pair.Value.name, move.moveName))
                {
                    continue;
                }

                var possibleId = GetAnimatorIndexFromName(pair.Value.motion as AnimationClip);
                if (possibleId.HasValue)
                {
                    move.AnimationId = possibleId.Value;
                } 

                if (move.AnimationId == 0)
                {
                    foreach (var m in fighterData.moves)
                    {
                        if (m.api_id == move.api_id)
                        {
                            move.AnimationId = m.AnimationId;
                        }
                    }
                }

                FighterAnimatorAction actionBehaviour = null;
                if (move.moveName.Contains("Charge)"))
                {
                    bool shouldAdd = true;
                    foreach (var behaviour in pair.Value.behaviours)
                    {
                        if (behaviour is FighterChargeAnimatorAction)
                        {
                            actionBehaviour = behaviour as FighterChargeAnimatorAction;
                            shouldAdd = false;
                            break;
                        }
                    }
                    if (shouldAdd)
                    {
                        actionBehaviour = pair.Value.AddStateMachineBehaviour<FighterChargeAnimatorAction>();
                    }
                    if (move.moveName.Contains("(Full"))
                    {
                        ((FighterChargeAnimatorAction)actionBehaviour).fullChargeMoveId = (move.api_id);
                    }
                    else if (move.moveName.Contains("(No"))
                    {
                        ApplyMove(move, ref actionBehaviour);
                    }
                }
                else if (move.moveName.Contains("(Hit"))
                {
                    bool shouldLeave = false;
                    foreach (var behaviour in pair.Value.behaviours)
                    {
                        if (behaviour is FighterMultiHitAnimatorAction)
                        {
                            ((FighterMultiHitAnimatorAction)behaviour).multiHitMoves.Add(move);
                            shouldLeave = true;
                            break;
                        }
                    }

                    if (shouldLeave)
                    {
                        continue;
                    }
                    actionBehaviour = pair.Value.AddStateMachineBehaviour<FighterMultiHitAnimatorAction>();
                    ((FighterMultiHitAnimatorAction)actionBehaviour).multiHitMoves.Add(move);
                    ApplyMove(move, ref actionBehaviour);
                }
                else if (move.moveName.Contains("Launcher)"))
                {
                    actionBehaviour = pair.Value.AddStateMachineBehaviour<FighterLauncherAnimatorAction>();
                    ApplyMove(move, ref actionBehaviour);
                }
                else
                {
                    actionBehaviour = pair.Value.AddStateMachineBehaviour<FighterAnimatorAction>();
                    ApplyMove(move, ref actionBehaviour);
                }

                actionBehaviour.FighterData = fighterData;
              /*  actionBehaviour.dataType = pair.Key.GetType().Name;
                actionBehaviour.serializedAction = JsonUtility.ToJson(pair.Key, true);*/
            }

            if (!containsDontMove && pair.Key.freezeMovement)
            {
                pair.Value.AddStateMachineBehaviour<StopMovement>();
            }
            else if (!pair.Key.freezeMovement)
            {
               // Debug.Log("Not freezing movement for " + pair.Key.actionName);
            }

            if (!containsDontRotate && pair.Key.freezeRotation)
            {
                pair.Value.AddStateMachineBehaviour<StopRotation>();
            }

            CleanUpStateBehaviours(pair.Value);
        }
    }

    //Get rid of duplicate behaviours on a state
    void CleanUpStateBehaviours(AnimatorState state)
    {
        Dictionary<Type, List<StateMachineBehaviour>> behaviourMap = new Dictionary<Type, List<StateMachineBehaviour>>();
        foreach(var b in state.behaviours)
        {
            if (!behaviourMap.ContainsKey(b.GetType()))
            {
                behaviourMap[b.GetType()] = new List<StateMachineBehaviour>();
            }

            behaviourMap[b.GetType()].Add(b);
        }

        List<StateMachineBehaviour> newBehaviours = new List<StateMachineBehaviour>();
        foreach(var behaviourList in behaviourMap.Values)
        {
            for(int i = 0; i < behaviourList.Count; i++)
            {
                var thisBehaviour = behaviourList[i];
                if (i == 0)
                {
                    newBehaviours.Add(thisBehaviour);
                }
                else if (thisBehaviour.GetType() == typeof(FighterAnimatorAction))
                {
                    bool matchFound = false;
                    for(int j = 0; j < i; j++)
                    {
                        if (((FighterAnimatorAction) behaviourList[j]).moveId == ((FighterAnimatorAction) thisBehaviour).moveId)
                        {
                            matchFound = true;
                            break;
                        }
                    }

                    if (!matchFound)
                    {
                        newBehaviours.Add(thisBehaviour);
                    } 
                }
            }
        }

        state.behaviours = newBehaviours.ToArray();
    }

    void ApplyMove(Move move, ref FighterAnimatorAction actionBehaviour)
    {
        actionBehaviour.moveId = move.api_id;
        actionBehaviour.moveName = move.moveName;
    }


    //TODO - This method is a holdover from old. We should get rid of it soon and replace with a more streamlined match system
    static RuntimeAnimatorCreatorResults MatchAnimatorStatesAndFighterData(AnimatorController controller)
    {
        UpdateProgressBar("Matching animator states with fighter data");

        Dictionary<ActionFrameData, AnimatorState> matched = new Dictionary<ActionFrameData, AnimatorState>();
        Dictionary<AnimatorState, Frame> stateFrames = new Dictionary<AnimatorState, Frame>();

        var animatorStates = GetAnimatorStates(controller);
        var actionData = fighterData.GetActionData();
        foreach (var data in actionData)
        {
            foreach (var state in animatorStates)
            {
                bool containsDebugAnimatorState = false;
                foreach (var b in state.behaviours)
                {
                    if (b is DebugAnimatorState)
                    {
                        (b as DebugAnimatorState).stateName = state.name;
                        containsDebugAnimatorState = true;
                        break;
                    }
                }

                if (!containsDebugAnimatorState)
                {
                    state.AddStateMachineBehaviour<DebugAnimatorState>().stateName = state.name;
                }

                if (data.actionName.ToLower().Contains(state.name.ToLower()))
                {
                    if (IsMatchException(state.name, data.actionName))
                    {
                        continue;
                    }
                    matched[data] = state;
                    stateFrames[state] = data.totalFrames;
                }

                else if (state.name == data.actionName
                    || (data is ChargeableAttackFrameData && state.name == data.actionName + " Attack")) //Do this first because multiple states might use the same motion
                {
                    matched[data] = state;
                    stateFrames[state] = data.totalFrames;
                }
                else if (data is ChargeableAttackFrameData && state.name == data.actionName + " Charge")
                {
                    var d = data as ChargeableAttackFrameData;
                    matched[data] = state;
                    stateFrames[state] = d.chargedFrameRange.minFrame;
                    ApplyMotionToState(state, d.chargeClip);
                    UpdateStateSpeed(state, d.chargedFrameRange.maxFrame);
                }
                else if (data.clip != null && state.motion == data.clip)
                {
                    matched[data] = state;
                    stateFrames[state] = data.totalFrames;
                }
                else if (data.actionName.ToLower().Contains(state.name.ToLower()))
                {
                    matched[data] = state;
                    stateFrames[state] = data.totalFrames;
                }
                else if (data is ShieldFrameData)
                {
                    var d = data as ShieldFrameData;
                    if (state.name == "Shield Hit")
                    {
                        matched[new ActionFrameData { clip = d.shieldHit }] = state;
                    }
                    else if (state.name == "Shield End")
                    {
                        matched[new ActionFrameData { clip = d.shieldDropped }] = state;
                    }
                    else if (state.name == "Shield Start")
                    {
                        matched[new ActionFrameData { clip = d.shieldStarted }] = state;
                    }
                }
                else
                {
                    // Debug.Log("No Match between " + state.name + " and " + data.actionName);
                }
            }
        }

        RuntimeAnimatorCreatorResults results = new RuntimeAnimatorCreatorResults();
        results.runtimeController = controller;
        //find any frames that have yet been paired, and if it can't, let somebody know
        foreach (var state in animatorStates)
        {
            if (!stateFrames.ContainsKey(state) && !matched.ContainsValue(state))
            {
                results.unmatchedStates.Add(state);
            }
        }


        //Display unpaired actions
        foreach (var data in actionData)
        {
            if (!matched.ContainsKey(data))
            {
                results.unmatchedActions.Add(data);
            }
        }

        results.matched = matched;
        results.stateFrames = stateFrames;
        return results;
    }

    static void ApplyMotionToState(AnimatorState state, Motion motion)
    {
        if (motion != null)
        {
            state.motion = motion;
        }
    }


    public static void UpdateStateSpeed(AnimatorState state, Frame framesCount)
    {
        if (state.motion != null && framesCount.numFrames > 0 && state.motion is AnimationClip)
        {
            var clip = (AnimationClip)state.motion;
           // Debug.Log("State " + state.name + " Speed: " + state.speed + "-> " + (clip.length * 60 / framesCount.value));
            state.speed = clip.length * 60 / framesCount.value;
            if (float.IsNaN(state.speed) || float.IsInfinity(state.speed))
            {
                Debug.LogError("Invalid Speed for " + state.name, state);
            }
        }
    }

    static bool IsMatchException(string state, string action)
    {
        if (action.Contains("Shield Breaker") && state.Trim().ToLower() == "shield")
            return true;
        return false;
    }

    void UpdateRootMotion()
    {
        foreach (var state in GetAnimatorStates(setupController))
        {
            if (state.motion != null && (state.motion is AnimationClip))
            {
                state.motion = UpdateClip(state.motion as AnimationClip);
            }
        }
        EditorUtility.ClearProgressBar();
    }

    public static List<ChildAnimatorState> GetChildAnimatorStates(AnimatorStateMachine stateMachine)
    {
        List<ChildAnimatorState> states = new List<ChildAnimatorState>();
        foreach (var s in stateMachine.states)
        {
            //    if (s.state.motion != null)
            {
                states.Add(s);
            }
        }

        foreach (var s in stateMachine.stateMachines)
        {
            states = states.Concat(GetChildAnimatorStates(s.stateMachine)).ToList();
        }

        return states;
    }

    public static List<AnimatorState> GetAnimatorStates(AnimatorStateMachine stateMachine)
    {
        List<AnimatorState> states = new List<AnimatorState>();

        foreach (var c in GetChildAnimatorStates(stateMachine))
        {
            states.Add(c.state);
        }
        
        return states;
    }

    public static List<AnimatorState> GetAnimatorStates(AnimatorController controller)
    {
        return GetAnimatorStates(controller.layers[0].stateMachine);
    }

    public static List<AnimatorStateMachine> GetAnimatorStateMachines(AnimatorController controller)
    {
        return GetAnimatorStateMachines(controller.layers[0].stateMachine);
    }

    public static List<AnimatorStateMachine> GetAnimatorStateMachines(AnimatorStateMachine stateMachine)
    {
        List<AnimatorStateMachine> stateMachines = new List<AnimatorStateMachine>();
        foreach (var s in stateMachine.stateMachines)
        {
            stateMachines.Add(s.stateMachine);
            stateMachines = stateMachines.Concat(GetAnimatorStateMachines(s.stateMachine)).ToList();
        }

        return stateMachines;
    }

    ushort? GetAnimatorIndexFromName(AnimationClip clip)
    {
        if (clip != null && clip.name.Contains("_"))
        {
            var split = clip.name.Split('_');
            ushort possibleAnimationId = 0;
            if (ushort.TryParse(split[split.Length - 1], out possibleAnimationId))
            {
                return possibleAnimationId;
            }
        }

        return null;
    }

    static void UpdateAnimatorParameters(AnimatorController controller)
    {
        UpdateInputParameters(controller);
        UpdateSmashStateParameters(controller);
        EditorUtility.ClearProgressBar();
    }

    static List<string> safeParamNames = new List<string>(new string[] { "JumpSquatTriggered" });
    static void UpdateInputParameters(AnimatorController controller)
    {
        IEnumerable<string> allButtonActions = new List<string>();
        IEnumerable<string> allAxisActions = new List<string>();
        foreach(var grim in Resources.LoadAll<GeniousRewiredMangerContainer>(""))
        {
            allButtonActions = allButtonActions.Concat(grim.actions.AllButtonNames.Except(grim.actions.NotAnimatorActionNames));
            allAxisActions = allAxisActions.Concat(grim.actions.AllAxisNames.Except(grim.actions.NotAnimatorActionNames));
        }
        UpdateProgressBar("Updating Input Animator parameters");
        var sorted = allButtonActions.OrderBy((x) => x).Concat(allAxisActions.OrderBy((x) => x)); ;

        foreach (var action in sorted)
        {
            foreach (var parameter in controller.parameters)
            {
                if ((parameter.name.StartsWith(action)  || parameter.name.StartsWith("Input_" + action) || parameter.name.StartsWith("InputAxis_" + action) || parameter.name.StartsWith("InputButton_" + action)) && !safeParamNames.Contains(parameter.name))// || parameter.name == j.name + "Started)
                {
                    controller.RemoveParameter(parameter);
                }
            }

            bool alreadyHandled = false;
            if (allAxisActions.Contains(action))
            {
                controller.AddParameter("InputAxis_" + action, AnimatorControllerParameterType.Float);
            }
            else
            {
                controller.AddParameter("InputButton_" + action, AnimatorControllerParameterType.Bool);

            }

            controller.AddParameter("Input_" + action + "Started", AnimatorControllerParameterType.Trigger);
            controller.AddParameter("Input_" + action + "Ended", AnimatorControllerParameterType.Trigger);
            controller.AddParameter("Input_" + action + "TimePressed", AnimatorControllerParameterType.Float);
        }
        EditorUtility.ClearProgressBar();
    }

    static void UpdateSmashStateParameters(AnimatorController controller)
    {
        UpdateProgressBar("Updating Smash State Animator parameters");

        //Add boolean params for each SmashState
        //TODO - Add an attribute for updating animator with things from here

        foreach (var stateType in AFighterState.GetAllFighterStateTypes())// Enum.GetNames(typeof(FighterController.FighterStateEnum)))
        {
            var state = stateType.Name;
            bool contains = false;
            foreach (var parameter in controller.parameters)
            {
                if (!safeParamNames.Contains(parameter.name) && (parameter.name == "State_" + state || parameter.name == state))
                {
                    controller.RemoveParameter(parameter);

                   // contains = true;
                    break;
                }
            }

            if (!contains)
            {
                controller.AddParameter("State_" + state, AnimatorControllerParameterType.Bool);
            }
        }

        /*
        var names = Enum.GetNames(typeof(SmashBrosPlayerController.SmashPlayerState)).ToList();
        foreach (var p in controller.parameters)
        {
            if (names.Contains(p.name))
            {
                names.Remove(p.name);
            }
        }

        foreach (var n in names)
        {
            controller.AddParameter(n, AnimatorControllerParameterType.Bool);
        }*/
    }
    AnimationClip CopySmashClip(AnimationClip orgClip, string baseName, bool addAutoUpdatedPrefix = true)
    {
        var animatorId = GetAnimatorIndexFromName(orgClip);
        var suffix = animatorId.HasValue ? "_" + animatorId.ToString() : "";
        var prefix = "(Auto Updated)";
        if (!baseName.StartsWith(prefix))
        {
            baseName = prefix + baseName;
        }

        if (!baseName.EndsWith(suffix))
        {
            baseName += suffix;
        }

        string finalClipPath = AssetDatabase.GetAssetPath(updatedAnimationsFolder) + "/" + baseName + ".anim";
        return CopyClip(orgClip, finalClipPath);
    }

    public static AnimationClip CopyClip(AnimationClip orgClip, string finalClipPath)
    {
        SerializedObject serializedClip = new SerializedObject(orgClip);
        //Save the clip
        AnimationClip placeClip = new AnimationClip();
        EditorUtility.CopySerialized(orgClip, placeClip);
        if (AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(finalClipPath) != null)
        {
            AssetDatabase.DeleteAsset(finalClipPath);
            AssetDatabase.Refresh();
        }

        AssetDatabase.CreateAsset(placeClip, finalClipPath);
        AssetDatabase.Refresh();
        return AssetDatabase.LoadAssetAtPath<AnimationClip>(finalClipPath);
    }

    Motion UpdateClip(AnimationClip clip)
    {
        UpdateProgressBar("Updating " + clip.name);

        if (clip.name.StartsWith("(Auto Updated)") && !reupdateConvertedClips)
        {
            return clip;
        }

        clip = CopySmashClip(clip, clip.name);// parentName);
        List<EditorCurveBinding> bindings = new List<EditorCurveBinding>();
        float lastKeyFrame = Mathf.NegativeInfinity;

        foreach (var binding in AnimationUtility.GetCurveBindings(clip))
        {
            if (binding.path == transformRootName && binding.propertyName.StartsWith("m_LocalPosition"))
            {
                bindings.Add(binding);
            }
        }
        foreach (var binding in bindings)
        {
            AnimationCurve curve = AnimationUtility.GetEditorCurve(clip, binding);
            lastKeyFrame = Mathf.Max(lastKeyFrame, curve.keys[curve.keys.Length - 1].time);

            if (removeLastKeyIfSameAsFirst)
            {
                if (curve.keys[0].value == curve.keys[curve.keys.Length - 1].value)
                {
                    curve.RemoveKey(curve.keys.Length - 1);
                }
            }

            string suffix = binding.propertyName.Substring(binding.propertyName.Length - 1);
            if (adjustRotationOfTransform)
            {
                if (suffix == "z")
                {
                    suffix = "y";
                    for (int i = 0; i < curve.keys.Length; i++)
                    {
                        Keyframe newKeyframe = curve.keys[i];
                        newKeyframe.value = -newKeyframe.value;
                        curve.MoveKey(i, newKeyframe);
                    }
                }
                else if (suffix == "y")
                {
                    suffix = "z";
                }
            }

            EditorCurveBinding newBinding = EditorCurveBinding.FloatCurve("", typeof(FighterModel), "animationMovementOffset." + suffix);
            AnimationUtility.SetEditorCurve(clip, newBinding, curve);
            AnimationUtility.SetEditorCurve(clip, binding, null);
            //                clip.AddEvent()
        }

        var evts = new List<AnimationEvent>(AnimationUtility.GetAnimationEvents(clip));
        AnimationEvent evt = new AnimationEvent();
        evt.functionName = "OnMovingAnimationStarted";
        evts.Add(evt);
        evt = new AnimationEvent();
        evt.functionName = "OnMovingAnimationEnded";
        evt.time = lastKeyFrame;
        evts.Add(evt);
        AnimationUtility.SetAnimationEvents(clip, evts.ToArray());
        bindings.Clear();
        return clip;
    }

    static int i = 0;
    static void UpdateProgressBar(string text, int? min = null, int? max = null)
    {
        float val = min.HasValue ? min.Value / max.Value : (float)++i / 10f;
        EditorUtility.DisplayProgressBar("Character Controller Updater", text, val);
    }

    void UpdateUnsyncedStates()
    {
        unsyncedStates = new List<UpdateableAnimatorState>();
        if (setupController != null)
        {
            foreach (var s in GetAnimatorStates(setupController.layers[0].stateMachine))
            {
                bool updated = false;
                if (s.motion == null)
                {
                    if (referenceController != null)
                    {
                        foreach(var s2 in GetAnimatorStates(referenceController))
                        {
                            if (s2.name == s.name)
                            {
                                updated = true;
                                unsyncedStates.Add(new UpdateableAnimatorState(s, s2));
                                break;
                            }
                        }
                    }

                    if (!updated)
                    {
                        unsyncedStates.Add(new UpdateableAnimatorState(s));
                    }
                }
            }
        }
    }

    AnimatorController referenceController;
    void DisplayUnsyncedStates()
    {
        if (unsyncedStates != null && unsyncedStates.Count > 0)
        {
            EditorGUILayout.LabelField("Empty States:", EditorStyles.boldLabel);
            if (GUILayout.Button("Update Unsynced States [Setup]"))
            {
                for (int i = 0; i < unsyncedStates.Count; i++)
                {
                    var u = unsyncedStates[i];
                    if (u.newMotion != null)
                    {
                        Debug.Log("Replace with " + AssetDatabase.GetAssetPath(u.newMotion));
                        /*ModelImporter modelImporter = (ModelImporter)AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(u.newMotion));
                        ModelImporterClipAnimation[] clipAnimations = modelImporter.clipAnimations;
                        Debug.Log(clipAnimations.Length + "," + modelImporter, modelImporter);
                        foreach (ModelImporterClipAnimation c in clipAnimations)
                        {
                            //Debug.Log("Clip: " + c.name);/*
                            if (c.name.Split('_')[0] == motionName)
                            {
                                c.loopTime = motion.isLooping;
                            }
                            */

                        var assetPath = AssetDatabase.GetAssetPath(u.newMotion);
                        AssetDatabase.ImportAsset(assetPath);
                        // or get animation Clip
                        UnityEngine.Object[] objects = AssetDatabase.LoadAllAssetsAtPath(assetPath);

                        foreach (UnityEngine.Object obj in objects)
                        {
                            AnimationClip c = obj as AnimationClip;
                            if (c != null && !c.name.Contains("preview"))// && c.name.Split('_')[0] == motionName)
                            {
                                if (u.state.motion != null)
                                {
                                    CustomAnimationClipSettings.SetLooping(c, u.state.motion.isLooping);
                                }
                                u.state.motion = u.newMotion;
                                unsyncedStates.RemoveAt(i);
                                i--;
                                break;
                            }
                        }
                    }
                }
            }
            EditorGUILayout.BeginHorizontal();
            if (referenceController != null)
            {
                EditorGUILayout.LabelField(fighterData.characterName + " [NEW]");
                EditorGUILayout.LabelField(referenceController.name + " [REFERENCE]");
            }
            EditorGUILayout.EndHorizontal();
            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

            GUIStyle[] styles = new GUIStyle[2] { new GUIStyle(), new GUIStyle() };
            styles[0].normal.background = MakeTex(2, 2, Color.gray);
            //styles[1].normal.background = MakeTex(2, 2, Color.white);
            int index = 0;
            EditorGUILayout.BeginVertical();
            foreach(var u in unsyncedStates.OrderBy(x => x.state.name))// (int i = 0; i < unsyncedStates.Count; i++)
            {
               // var u = unsyncedStates[i];
                /* if (u.newMotion != null)
                 {
                     Debug.Log("Replace with " + AssetDatabase.GetAssetPath(u.newMotion));
                     //u.state.motion = unsyncedStates[i].newMotion;
                     unsyncedStates.RemoveAt(i);
                     i--;
                 }
                 else*/
                {
                    EditorGUILayout.BeginHorizontal(styles[index++ % 2]);
                    //string name = u.state.motion == null || String.IsNullOrEmpty(u.state.motion.name) ? "No Motion Attached" : u.state.motion.name;
                    u.newMotion = EditorGUILayout.ObjectField("[New]" + u.state.name, u.newMotion, typeof(Motion)) as Motion;

                    if (referenceController != null)
                    {
                        if (u.referenceMotion != null)
                        {
                            EditorGUILayout.ObjectField(u.state.name, u.referenceMotion, typeof(Motion));
                        }
                        else
                        {
                            var label = u.state.name;
                            EditorGUILayout.LabelField(u.state.name + "\t\t\tNo Reference Available");
                        }
                    }
                  
                    EditorGUILayout.EndHorizontal();
                }
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.EndScrollView();
        }
    }

    AnimatorController Clone()
    {
        var ogFilePath = AssetDatabase.GetAssetPath(animatorToClone);
        var split = ogFilePath.Split('/');
        string newFile = "";
        for (int i = 0; i < split.Length - 2; i++)
        {
            newFile += split[i] + "/";
        }

        newFile = newFile.Substring(0, newFile.Length - 1);

        if (AssetDatabase.LoadAssetAtPath<DefaultAsset>(newFile + "/" + fighterData.displayName) == null)
        {
            AssetDatabase.CreateFolder(newFile, fighterData.displayName);
        }
        newFile += "/" + fighterData.name;
        newFile += "/" + fighterData.name + " Controller [Setup].controller";

        string tempFilePath = newFile.Replace(".controller", "_temp.controller");
        bool tempFileAvailable = AssetDatabase.CopyAsset(newFile, tempFilePath);
        AssetDatabase.DeleteAsset(newFile);
        if (!AssetDatabase.CopyAsset(ogFilePath, newFile))
        {
            Debug.LogError("Error copying asset from " + ogFilePath + " to " + newFile);
            return null;
        }
        else
        {
            Debug.Log("Copied asset to " + newFile, AssetDatabase.LoadAssetAtPath<AnimatorController>(newFile));
        }
        AssetDatabase.Refresh();
        var newController = AssetDatabase.LoadAssetAtPath<AnimatorController>(newFile);
        List<string> newCharacterAnimtionPaths = new List<string>();
        foreach (var guid in AssetDatabase.FindAssets("t:Animation", new string[] { AssetDatabase.GetAssetPath(animationPlacementFolder) }))
        {
            newCharacterAnimtionPaths.Add(AssetDatabase.GUIDToAssetPath(guid));
        }

        foreach (var state in GetAnimatorStates(newController.layers[0].stateMachine))
        {
            bool updated = false;
            var motion = state.motion;
            if (motion != null)
            {
                var motionName = motion.name;
                var indexOf = motionName.IndexOf("@");
                if (indexOf >= 0)
                {
                    motionName = motionName.Substring(indexOf + 1);
                }
                indexOf = motionName.LastIndexOf("_");
                if (indexOf >= 0)
                {
                    motionName = motionName.Substring(0, indexOf);
                }

                string autoUpdatedPrefix = "(Auto Updated)";
                if (motionName.StartsWith(autoUpdatedPrefix))
                {
                    motionName = motionName.Substring(autoUpdatedPrefix.Length);
                }

                foreach (var possibleAnimPath in newCharacterAnimtionPaths)
                {
                    split = possibleAnimPath.Split('/');
                    split = split[split.Length - 1].Split('_');
                    split = split[0].Split('@');
                    var newAnim = split[1];

                    if (newAnim == motionName)
                    {
                        /* // Modify ModelImporter meta data and reimport the asset
                            Debug.Log("PossibleAnimPath: " + possibleAnimPath);
                            ModelImporter modelImporter = (ModelImporter)AssetImporter.GetAtPath(possibleAnimPath);
                            Debug.Log("IMPORTED PTH: " + modelImporter.assetPath);
                            ModelImporterClipAnimation[] clipAnimations = modelImporter.clipAnimations;
                            Debug.Log(clipAnimations.Length + ";");
                            foreach (ModelImporterClipAnimation c in clipAnimations)
                            {
                                if (c.name.Split('_')[0] == motionName)
                                {
                                    c.loopTime = motion.isLooping;
                                }
                            }
                            modelImporter.clipAnimations = clipAnimations;*/
                        AssetDatabase.ImportAsset(possibleAnimPath);
                        // or get animation Clip
                        UnityEngine.Object[] objects = AssetDatabase.LoadAllAssetsAtPath(possibleAnimPath);
                        foreach (UnityEngine.Object obj in objects)
                        {
                            AnimationClip c = obj as AnimationClip;
                            if (c != null && c.name.Split('_')[0] == motionName)
                            {
                                CustomAnimationClipSettings.SetLooping(c, state.motion.isLooping);
                                state.motion = c;
                                Debug.Log("Updated " + motionName + " to " + state.motion.name, c);
                                updated = true;
                                break;
                            }
                        }
                    }
                }
            }

            if (!updated)
            {
                Debug.Log("Couldn't automatically update " + state.name + "(" + (state.motion == null ? "" : state.motion.name) + ")");

                if (!restartFromScratch && tempFileAvailable)
                {
                    AnimatorController lastController = AssetDatabase.LoadAssetAtPath<AnimatorController>(tempFilePath);
                    foreach (var s in GetAnimatorStates(lastController.layers[0].stateMachine))
                    {
                        if (s.name == state.name && s.motion != state.motion && s.motion != null)
                        {
                           // Debug.Log("using old motion for " + state.name);
                            state.motion = s.motion;
                            updated = true;
                            break;
                        }
                    }
                }

                if (!updated)
                {
                    state.motion = null;
                }
            }
        }

        if (tempFileAvailable)
        {
            AssetDatabase.DeleteAsset(tempFilePath);
            AssetDatabase.Refresh();
        }

        UpdateInputParameters(newController);
        return newController;
    }

    Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];

        for (int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }

    bool GUIButton(string text)
    {
        Debug.ClearDeveloperConsole();
        return GUILayout.Button(text);
    }

    class BrokenTransition
    {
        public UnityEngine.Object reference;
        public AnimatorTransitionBase brokenTransition;
        public AnimatorCondition brokenCondition;
        public string newTransitionParameter;
        public Dictionary<string, int> newTransitionIndex { get; protected set; }
        public BrokenTransition(UnityEngine.Object s, AnimatorTransitionBase brokenStateTransition, AnimatorCondition _brokenCondition)
        {
            newTransitionParameter = null;
            brokenCondition = _brokenCondition;
            newTransitionIndex = new Dictionary<string, int>();
            reference = s;
            brokenTransition = brokenStateTransition;
        }

        public void SetTransitionIndex(string s, int val)
        {
            newTransitionIndex[s] = val;
        }

        public int GetTransitionIndex(string s)
        {
            if (newTransitionIndex.ContainsKey(s))
            {
                return newTransitionIndex[s];
            }

            return 0;
            
        }
    }

    void RequestSaveProject()
    {
        if (EditorUtility.DisplayDialog("Save Project?","Would you like to save the project now, in order to maintain references and preserve assets?", "Yes", "No"))
        {
            EditorApplication.ExecuteMenuItem("File/Save Project");
        }
    }

    class UpdateableAnimatorState
    {
        public AnimatorState state;
        public Motion newMotion;
        public Motion referenceMotion;
        public UpdateableAnimatorState(AnimatorState s)
        {
            state = s;
            referenceMotion = state.motion;
            s.motion = null;
            newMotion = null;
        }

        public UpdateableAnimatorState(AnimatorState s, AnimatorState referenceState) : this(s)
        {
            referenceMotion = referenceState.motion;
        }
    }

    class RuntimeAnimatorCreatorResults
    {
        public AnimatorController runtimeController;
        /// <summary>
        /// How many frames each state should be, based on the incoming FrameData for that move
        /// </summary>
        public Dictionary<AnimatorState, Frame> stateFrames = new Dictionary<AnimatorState, Frame>();
        /// <summary>
        /// All states and actions that have been automatically matched together
        /// </summary>
        public Dictionary<ActionFrameData, AnimatorState> matched = new Dictionary<ActionFrameData, AnimatorState>();
        /// <summary>
        /// All actions that were unable to be automatically matched with states
        /// </summary>
        public List<ActionFrameData> unmatchedActions = new List<ActionFrameData>();
        /// <summary>
        /// All states that were unable to be automatically matched with actions
        /// </summary>
        public List<AnimatorState> unmatchedStates = new List<AnimatorState>();
        public bool displayUnmatchedActions;
        public bool displayUnmatchedStates;
    }
}