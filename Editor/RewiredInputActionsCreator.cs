using Rewired;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEditor;
using UnityEngine;

[InitializeOnLoadAttribute]
public static class RewiredInputActionsCreator
{
    static Rewired.Editor.InputEditor rewiredEditor = null;
    static bool wasOpened;
    static Dictionary<InputAction, int> actions;// = new List<InputAction>();
    static InputManager manager;
    static RewiredInputActionsCreator()
    {
        EditorApplication.update += () =>
        {
            if (rewiredEditor == null)
            {
                EditorWindow.FocusWindowIfItsOpen<Rewired.Editor.InputEditor>();
            }
            Rewired.Editor.InputEditor thisRewiredEditor = EditorWindow.focusedWindow as Rewired.Editor.InputEditor;
            if (rewiredEditor == null && thisRewiredEditor != null)
            {
                rewiredEditor = thisRewiredEditor;
                wasOpened = true;
                manager = Selection.activeGameObject?.GetComponent<InputManager>();

                actions = new Dictionary<InputAction, int>();
                foreach( var b in manager?.userData.GetActions_Copy())
                {
                    actions[b] = b.categoryId;
                }
            }
            else if (rewiredEditor == null && wasOpened)
            {
                var theseActions = manager.userData.GetActions_Copy();
                bool found = theseActions != null && theseActions.Count == actions.Count; //not the same size?
                if (found)
                {
                    foreach (var a in theseActions)
                    {
                        found = false;
                        foreach (var b in actions)
                        {
                            if (a.name == b.Key.name)
                            {
                                if (a.categoryId == b.Value)//if category changed, register it as a change
                                {
                                    found = true;
                                }
                                break;
                            }
                        }
                        if (!found)
                        {
                            break;
                        }
                    }
                }

                if (found)
                {
                    Debug.Log("No change made that requires a rebuild of RewiredInputActions.cs");
                }
                else
                {
                    Debug.Log("Rebuilding RewiredInputActions");
                    CreateInputActionClasses();
                }
                //Debug.Log("Editor Closed: " + Selection.activeGameObject + " vs " + rewiredEditor);
                wasOpened = false;
            }
        };
    }
    public const char scriptSplitChar = '*';
    public const string scriptsToCreateKey = "SCRIPTS_TO_CREATE";
    public const string shouldCreateScriptKey = "SHOULD_CREATE_SCRIPT";
    public const string newScriptSuffix = "Actions";
    [MenuItem("Genious/Create RewiredInputActions.cs")]
    public static void CreateInputActionClasses()
    {
        EditorPrefs.SetBool(shouldCreateScriptKey, true);
        foreach (var grim in Resources.LoadAll<GeniousRewiredMangerContainer>(""))
        {
            CreateScript(AssetDatabase.GetAssetPath(grim));
        }

        UnityEditor.AssetDatabase.Refresh();
    }

    [UnityEditor.Callbacks.DidReloadScripts]
    private static void OnScriptsReloaded()
    {
        try
        {
            if (EditorPrefs.GetBool(shouldCreateScriptKey))
            {
                foreach (var grim in Resources.LoadAll<GeniousRewiredMangerContainer>(""))
                {
                    var path = AssetDatabase.GetAssetPath(grim);
                    var modelRootGO = (GameObject)AssetDatabase.LoadMainAssetAtPath(path);
                    string scriptPath = path.Substring(0, path.IndexOf('.')).Replace("Resources/" + modelRootGO.name, GetTypeFromFileName(modelRootGO.name)) + newScriptSuffix + ".cs";
                    Debug.Log("ScriptPath: " + scriptPath);
                    var newType = AssetDatabase.LoadAssetAtPath<MonoScript>(scriptPath).GetClass();

                    GameObject instanceRoot = PrefabUtility.InstantiatePrefab(modelRootGO) as GameObject;
                    var oldActions = instanceRoot.GetComponentsInChildren<RewiredInputActions>();
                    for (int i = 0; i < oldActions.Length; i++)
                    {
                        GameObject.DestroyImmediate(oldActions[i]);
                    }
                    instanceRoot.GetComponent<GeniousRewiredMangerContainer>().actions = instanceRoot.GetOrAddComponent(newType) as RewiredInputActions;
                    PrefabUtility.SaveAsPrefabAsset(instanceRoot, path);
                    Debug.Log("Added component [" + newType + "] to [" + instanceRoot + "]");
                    GameObject.DestroyImmediate(instanceRoot);
                }
            }
        }
        catch(Exception)
        {

        }
        finally
        {
            EditorPrefs.SetBool(shouldCreateScriptKey, false);
        }
    }

    public static void AddOnce(this Action a, Action actionToRunOnce)
    {
        Action toAdd = () => { };
        toAdd += () =>
        {
            actionToRunOnce();
            a -= toAdd;// actionToRunOnce;
        };
        a += toAdd;
    }

    public static void AddOnce<T>(this Action<T> a, Action<T> actionToRunOnce)
    {
        Action<T> toAdd = (val) => { };
        toAdd += (val) =>
        {
            actionToRunOnce(val);
            a -= toAdd;// actionToRunOnce;
        };

        a += toAdd;
    }

    public static void CreateAndAssignScripts(string[] toCreate)
    {
        if (!EditorApplication.isPlaying)
        {
            Debug.LogError("Can't create script when not in play mode");
            return;
        }
        foreach (var prefab in toCreate)
        {
            var split = prefab.Split('.');
            CreateScript(prefab);
        }

        UnityEditor.AssetDatabase.Refresh();
        EditorApplication.isPlaying = false;
    }

    static string GetTypeFromFileName(string fileNameWithoutExtension)
    {
        TextInfo myTI = new CultureInfo("en-US", false).TextInfo;
        return fileNameWithoutExtension.IndexOf(' ') >= 0 ? myTI.ToTitleCase(fileNameWithoutExtension).Replace(" ", "") : fileNameWithoutExtension;
    }

    static void CreateScript(string incomingPath)
    {
        string className = GetTypeFromFileName(Path.GetFileNameWithoutExtension(incomingPath)) + newScriptSuffix;

        Debug.Log("Creating " + className + " from [" + incomingPath + "]");
        string contents = "using Rewired;\npublic class " + className + " : RewiredInputActions{\n"; //RewiredInputActions : MonoSingleton<RewiredInputActions>{
        string allButtonActionsArray = "\tpublic static string[] AllButtonActions = new string[] {";
        string allAxisActionsArray = "\tpublic static string[] AllAxisActions = new string[] {";
        string notAnimatorActionsArray = "\tpublic static string[] NotAnimatorActions = new string[] {";

        string allButtonNamesOverride = "\tpublic override string[] AllButtonNames {get {return AllButtonActions;}}\n";
        string allAxisNamesOverride = "\tpublic override string[] AllAxisNames {get {return AllAxisActions;}}\n";
        string notAnimatorActionsOverride = "\tpublic override string[] NotAnimatorActionNames {get {return NotAnimatorActions;}}\n";

        string definitions = "";
        string awakeDefinition = "\tprotected override void Awake()\n\t{\n\t\tbase.Awake();\n\t\tforeach(var action in ReInput.mapping.Actions)\n\t\t{\n";
        InputManager inputManager = AssetDatabase.LoadAssetAtPath<InputManager>(incomingPath); ;

        var allActions = inputManager.userData.GetActions_Copy();
        HashSet<int> notAnimatorActionCategoryIds = new HashSet<int>();
        foreach(var notAnimatorActionCategoryName in GeniousInputManager.NOT_ANIMATOR_ACTION_CATEGORY_NAME)
        {
            notAnimatorActionCategoryIds.Add(inputManager.userData.GetActionCategory(notAnimatorActionCategoryName).id);
        }
        bool buttons = false; ;
        bool axes = false;
        bool notAnimatorActions = false;
        foreach (var action in allActions)//ReInput.mapping.Actions)
        {
            var nameStrippedSpaces = action.name.Replace(" ", "");
            string toAppend = "\"" + nameStrippedSpaces + "\",";
            if (action.type == InputActionType.Axis)
            {
                allAxisActionsArray += toAppend;
                axes = true;
            }
            else if (action.type == InputActionType.Button)
            {
                allButtonActionsArray += toAppend;
                buttons = true;
            }
            else
            {
                Debug.LogError("Case not handled");
            }

            if (notAnimatorActionCategoryIds != null && notAnimatorActionCategoryIds.Contains(action.categoryId))// == notAnimatorActionCategoryIds.id)
            {
                notAnimatorActionsArray += toAppend;
                notAnimatorActions = true;
            }

            definitions += "\tpublic static InputAction " + nameStrippedSpaces + " {get{return Get(\"" + action.name + "\", typeof(" + className + "));}}\n";//; protected set;}\n";
            awakeDefinition += "\t\t\tif (action.name == \"" + action.name + "\"){" + nameStrippedSpaces + "=action;}\n";
        }

        if (buttons)
        {
            allButtonActionsArray = allButtonActionsArray.Substring(0, allButtonActionsArray.Length - 1);
        }
        if (axes)
        {
            allAxisActionsArray = allAxisActionsArray.Substring(0, allAxisActionsArray.Length - 1);
        }

        if (notAnimatorActions)
        {
            notAnimatorActionsArray = notAnimatorActionsArray.Substring(0, notAnimatorActionsArray.Length - 1);
        }
        allButtonActionsArray += "};\n";
        allAxisActionsArray += "};\n";
        notAnimatorActionsArray += "};\n";

        contents += allButtonActionsArray + allButtonNamesOverride 
            + allAxisActionsArray + allAxisNamesOverride
            + notAnimatorActionsArray + notAnimatorActionsOverride
            + "\n"
            + definitions
            + "}";// + awakeDefinition;

            //TextAsset script = new TextAsset(s);
        // UnityEditor.AssetDatabase.CreateAsset(script, "Assets/RewiredInputActions.cs");
        string destinationPath = Application.dataPath.Replace('/', Path.DirectorySeparatorChar);
        var split = incomingPath.Split('/');
        for (int i = 1; i < split.Length - 2; i++)
        {
            destinationPath += Path.DirectorySeparatorChar + split[i];
        }

        destinationPath += Path.DirectorySeparatorChar + className + ".cs";
        Debug.Log("Writing to path " + destinationPath);
        File.WriteAllText(destinationPath, contents);
        //File.WriteAllText(Application.dataPath + "/GeniousFighter/RewiredInputActions.cs", s);
    }
}