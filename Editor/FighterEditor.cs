using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System;
using UnityEngine;
using UnityEditor;

public class FighterEditor : EditorWindow
{

    public static string projectAssetPath = "Assets/resources/options/";
    private Editor editorWindow;
    private Type[] _optionsTypes;
    private bool _shouldRefresh = false;
    private bool _wasPlaying = false;

    public static Dictionary<UnityEngine.Object, Editor> _editors = new Dictionary<UnityEngine.Object, Editor>();

    void OnInspectorUpdate()
    {
        Repaint();
    }

    void Update()
    {
        if (EditorApplication.isCompiling) {
            _shouldRefresh = true;
        }
        else if (_shouldRefresh) {
            RefreshList();
            _shouldRefresh = false;
        }

        if (EditorApplication.isPlaying && !_wasPlaying) {
            SetModeAll(OptionsViewMode.Scene);
            _wasPlaying = true;
        }

        if (!EditorApplication.isPlaying && _wasPlaying) {
            SetModeAll(OptionsViewMode.Asset);
            _wasPlaying = false;
        }
    }
    Color primaryColor = new Color(0, .67f, 1f, 1);
    Color primaryColorDim = new Color(.5f, .35f, 0, 1);

    private Vector2 scrollPos = Vector2.zero;

    [MenuItem("Genious/Fighter Editor")]
    static void DisplaySmashData()
    {
        FighterEditor window = EditorWindow.GetWindow<FighterEditor>("Fighters");
    }

    List<FighterData> fighterData;

    void ShowFighterData()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

        primaryColorDim = primaryColor;
        primaryColorDim.a = .5f;

        EditorGUILayout.BeginVertical();

        GUIStyle style = new GUIStyle(EditorStyles.foldout);

        style.active = EditorStyles.boldLabel.active;
        style.font = EditorStyles.boldLabel.font;
        style.fontStyle = EditorStyles.boldLabel.fontStyle;
        style.fontSize = EditorStyles.boldLabel.fontSize;

        if (optionsTypes.Length == 0)
        {
            EditorGUILayout.HelpBox("No Options found in project. Create a class that extends Options to get started, i.e.:", MessageType.Info, true);
            EditorGUILayout.HelpBox("public class MyOptions : Options<MyOptions>", MessageType.None, true);
        }


        for(int j = 0; j < fighterData.Count; j++)// (var instance in )
        {

            FighterData fighter = fighterData[j] as FighterData;
            Debug.Log("Character: " + fighter.characterName);
            Type type = fighter.GetType();
            GUI.enabled = true;

            GUILayout.Box("", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });
            bool isOpen = EditorPrefs.GetBool("Foldout" + fighter.characterName);
            isOpen = EditorGUILayout.Foldout(isOpen, Humanize(fighter.characterName), style);
            EditorPrefs.SetBool("Foldout" + fighter.characterName, isOpen);

            if (!Application.isPlaying) EditorGUI.indentLevel++;

            try
            {
                if (isOpen)
                {
                    if (Application.isPlaying)
                    {
                        Rect rect = EditorGUILayout.GetControlRect();
                        rect.width /= 2;
                        GUI.color = GetMode(type) == OptionsViewMode.Scene ? Color.white : Color.grey;
                        GUI.enabled = Application.isPlaying;
                        if (GUI.Button(rect, new GUIContent("Scene")))
                        {
                            SetMode(type, OptionsViewMode.Scene);
                            ClearEditor(fighter);
                        }


                        rect.x += rect.width;
                        GUI.color = GetMode(type) == OptionsViewMode.Asset ? primaryColor : primaryColorDim;
                        GUI.enabled = true;
                        if (GUI.Button(rect, new GUIContent("Asset")))
                        {
                            SetMode(type, OptionsViewMode.Asset);
                            ClearEditor(fighter);
                        }

                        GUI.enabled = true;

                        GUI.color = GetMode(type) == OptionsViewMode.Asset ? primaryColor : Color.white;

                        EditorGUILayout.BeginVertical("box");

                        EditorGUILayout.Space();

                    }
                    else
                    {
                        EditorGUILayout.BeginVertical();
                    }

                    /*string path = projectAssetPath + type.Name + ".asset";
                    var instance = AssetDatabase.LoadAssetAtPath(path, type);*/
                    bool hasAsset = fighter != null;

                    if (Application.isPlaying && GetMode(type) == OptionsViewMode.Scene)
                    {
                        var comp = FindObjectOfType(type) as FighterData;
                        fighter = comp;
                    }

                    if (Application.isPlaying)
                    {
                        GUI.enabled = (GetMode(type) == OptionsViewMode.Scene && fighter != null);
                        EditorGUILayout.BeginHorizontal();

                        GUILayout.Label("WARNING - IN PLAY MODE. IDK WHAT THIS MEANS");
                        /*
                        if (GUILayout.Button("Revert", EditorStyles.miniButtonMid))
                        {
                            SaveValues(AssetDatabase.LoadAssetAtPath(path, type), instance);
                        }

                        if (GUILayout.Button("Apply", EditorStyles.miniButtonRight))
                        {
                            SaveValues(instance, AssetDatabase.LoadAssetAtPath(path, type));
                        }*/


                        EditorGUILayout.EndHorizontal();
                        GUI.enabled = true;
                        EditorGUILayout.Space();
                    }


                    GUI.color = Color.white;

                    if (fighter != null)
                    {
                        Editor editor = GetEditor(fighter);
                        editor.OnInspectorGUI();
                        //GUILayout.BeginHorizontal();
                        if (fighter is FighterData)
                        {
                            var data = (FighterData)fighter;

                            if (GUILayout.Button("Update Hitbox Data"))
                            {
                                if (data.atkdAsset == null)
                                {
                                    Debug.LogError("You must assign a atkdAsset in order to generate the hitbox data automatically");
                                }
                                else
                                {
                                    data.hitboxData = new Smash_Forge.ATKD(data.atkdAsset);
                                }
                            }

                            bool shouldDuplicate = GUILayout.Button("Duplicate");
                            if (newName == null)
                            {
                                newName = data.characterName + "_Copy";
                            }
                            newName = EditorGUILayout.TextField("New Character", newName);
                            if (shouldDuplicate)
                            {
                                AssetDatabase.CopyAsset(AssetDatabase.GetAssetPath(fighter), "Assets/" + newName + ".asset");
                                Debug.Log("Asset placed in the Assets folder of the project with the name [" + newName + "]");
                                AssetDatabase.Refresh();
                                fighterData = null;
                                break;
                            }
                        }

                        //  GUILayout.EndHorizontal();
                    }
                    else
                    {
                        Debug.Log("Instance is null fro some reason");
                    }

                    EditorGUILayout.EndVertical();

                }
            }
            catch (System.NullReferenceException ex)
            {
                Debug.LogWarning("NullReferenceException while accessing " + type.Name + "\n" + ex.ToString());
            }


            if (!Application.isPlaying) EditorGUI.indentLevel--;
        }

        EditorGUILayout.EndVertical();
        EditorGUILayout.EndScrollView();
    }

    void OnGUI()
    {
        if (fighterData == null)
        {
            fighterData = new List<FighterData>();
            Debug.Log("Loading");
            foreach(var a in Resources.LoadAll<FighterData>(""))//AssetDatabase.FindAssets("t:FighterData"))
            {
                fighterData.Add(a);//AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(a), typeof(FighterData)) as FighterData);
            }
        }
        ShowFighterData();
        return;
    }

    string newName;


    void SaveValues(UnityEngine.Object from, UnityEngine.Object to)
    {
        SerializedObject source = new SerializedObject(from);
        SerializedObject destination = new SerializedObject(to);


        SerializedProperty property = source.GetIterator();
        property.NextVisible(true);
        while (property.NextVisible(false))
        {
            destination.CopyFromSerializedProperty(property);
        }

        destination.ApplyModifiedProperties();
    }

    void CreateAsset(Type type)
    {
        CheckPath(); // TODO bookmark
        string path = projectAssetPath + type.Name + ".asset";
        var so = ScriptableObject.CreateInstance(type);

        AssetDatabase.CreateAsset(so, path);
        // TODO Might be a memory leak : Does the asset database reference or copy the scriptable object?
    }

    void ClearEditor(FighterData fighter)
    {
        Editor editor = null;
        if (_editors.TryGetValue(fighter, out editor)) {
            DestroyImmediate(editor);
            _editors.Remove(fighter);
        }
    }

    public static Editor GetEditor(UnityEngine.Object instance)
    {
        Editor editor = null;

        if (!_editors.TryGetValue(instance, out editor)) {
            editor = Editor.CreateEditor(instance);
            _editors[instance] = editor;
        }

        return editor;
    }

    OptionsViewMode GetMode(Type type)
    {
        return (OptionsViewMode)EditorPrefs.GetInt("ViewMode" + type.Name);
    }

    void SetMode(Type type, OptionsViewMode mode)
    {
        EditorPrefs.SetInt("ViewMode" + type.Name, (int)mode);
    }


    void SetModeAll(OptionsViewMode mode)
    {
        foreach (Type type in optionsTypes)
        {
            SetMode(type, mode);
        }

        _editors.Clear();
    }

    void CheckPath()
    {
        if (!Directory.Exists(Application.dataPath + "/resources")) {
            AssetDatabase.CreateFolder("Assets", "resources");
        }
        if (!Directory.Exists(Application.dataPath + "/resources/options")) {
            AssetDatabase.CreateFolder("Assets/resources", "options");
        }
    }

    void RefreshList()
    {
        _optionsTypes = GetAllSubTypes(typeof(Options<>));
    }


    public Type[] optionsTypes
    {
        get
        {
            if (_optionsTypes == null) {
                RefreshList();
            }

            return _optionsTypes;
        }
    }

    string Humanize(string name)
    {
        return name; // Seems good enough
    }

    static Type[] GetAllSubTypes(Type aBaseClass)
    {
        List<Type> result = new List<Type>();
        Assembly[] allAssemblies = System.AppDomain.CurrentDomain.GetAssemblies();
        foreach (var a in allAssemblies)
        {
            Type[] types = a.GetTypes();
            foreach (Type T in types)
            {
                if (aBaseClass != T && IsDescendant(aBaseClass, T))
                    result.Add(T);
            }
        }
        return result.ToArray();
    }

    static bool IsDescendant(Type baseType, Type checkType)
    {
        while (checkType != null && checkType != typeof(object))
        {
            // Ignore generic composition
            var current = checkType.IsGenericType ? checkType.GetGenericTypeDefinition() : checkType;
            if (baseType == current) {
                return true;
            }
            checkType = checkType.BaseType;
        }
        return false;
    }
}

enum OptionsViewMode
{
    Asset,
    Scene
}