using UnityEditor;
using UnityEngine;

public class SmashMaterialUpdater : EditorWindow
{
    [MenuItem("Genious/Smash Material Updater")]
    public static void ShowWindow()
    {
        GetWindow<SmashMaterialUpdater>(false, "Update Materials", true);
    }

    DefaultAsset materialFolder;
    GameObject ogPrefab;
    public void OnGUI()
    {
        materialFolder = EditorGUILayout.ObjectField("Materials Folder", materialFolder, typeof(DefaultAsset)) as DefaultAsset;
        ogPrefab = EditorGUILayout.ObjectField("Prefab", ogPrefab, typeof(GameObject)) as GameObject;
        if (GUILayout.Button("Update"))
        {
            var prefab = PrefabUtility.InstantiatePrefab(ogPrefab) as GameObject;
            foreach (var r in prefab.GetComponentsInChildren<Renderer>())
            {

                // for(int i = 0; i < r.sharedMaterials.Length; i++) 
                {
                    var m = r.sharedMaterial;// r.sharedMaterials[i];
                    if (m != null)
                    {
                        var path = AssetDatabase.GetAssetPath(materialFolder) + "/" + m.name + ".mat";
                        var newMaterial = AssetDatabase.LoadAssetAtPath<Material>(path);
                        if (newMaterial != null)
                        {
                            r.sharedMaterial = newMaterial;// sharedMaterials[i] = newMaterial;
                            Debug.Log(newMaterial.name, newMaterial);
                        }
                        else
                        {
                            Debug.Log("No luck with " + path);
                        }
                    }
                }

            }

            PrefabUtility.ReplacePrefab(prefab, ogPrefab, ReplacePrefabOptions.ConnectToPrefab);
            AssetDatabase.SaveAssets();
            // prefab.AddComponent<Gamekit3D.AmbientAudio>();
            //prefab.name = "Updated Prefab Name";
            AssetDatabase.Refresh();
        }
    }
}