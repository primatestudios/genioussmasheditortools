using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

public class FighterCreator : EditorWindow
{
    const string FIGHTER_MATERIAL_TEMPLATE_PATH = "Assets/GeniousArena/Art/Materials/Fighter Material Template.mat";
    const string RESOURCES_PATH = "Resources";
    const string ANIMATION_CONTROLLERS_PATH = "Animation Controllers";
    const string EXPORTED_CLIPS_PATH = "Exported Clips";
    const string AUTO_UPDATED_CLIPS_PATH = "Auto Updated Clips";
    public const string MANUALLY_EDITED_CLIPS_PATH = "Manually Edited Clips";
    const string HITBOX_ATKD_PATH = "Hitbox ATKD";
    const string MODEL_PATH = "Model";
    const string MESH_PATH = "Model/Mesh";
    const string MATERIALS_PATH = "Model/Materials";
    const string TEXTURES_PATH = "Model/Textures";
    const string PREFABS_PATH = "Prefabs";
    const string PORTRAIT_PATH = "Portraits";
    public TextAsset readmeTemplate;
    public TextAsset packageJsonTemplate;
    public GameObject shieldPrefab;
    const float unityAttributeHeightScale = 0.0100032571428571f;//The ratio of Smash height to Unity height
    // * x = (175 * 0.01f) /boundAt1  175 height and 21.882 BoundAt1 = 12.5 (0r 0.08) 175/1.75057= x/21.882/ => x = bounds * ()
    [MenuItem("Genious/Fighter Creator")]
    public static void ShowWindow()
    {
        GetWindow<FighterCreator>(false, "Fighter Creator", true);
    }

    string newName;
    DefaultAsset newCharacterFolder;
    bool getFromSmash;
    string smashCharName;
    FighterData dataToCopy;
    AnimatorController setupAnimatorControllerToCopy;
    DefaultAsset animationClipFolder;
    Object mesh;
    Material sampleMaterial;
    Sprite portrait;

    public void Awake()
    {
        sampleMaterial = AssetDatabase.LoadAssetAtPath<Material>(FIGHTER_MATERIAL_TEMPLATE_PATH);
    }

    bool autoAssignedName = false;
    public void OnGUI()
    {
        EditorGUIUtility.labelWidth = 250;
        mesh = EditorGUILayout.ObjectField("Mesh", mesh, typeof(Object)) as Object;
        sampleMaterial = EditorGUILayout.ObjectField("Sample Material", sampleMaterial, typeof(Material)) as Material;

        newName = EditorGUILayout.TextField("New Character Name", newName);
        if (!autoAssignedName && mesh != null && string.IsNullOrWhiteSpace(newName))
        {
            newName = mesh.name;
            autoAssignedName = true;
        }
        newCharacterFolder = EditorGUILayout.ObjectField("New Character Folder", newCharacterFolder, typeof(DefaultAsset)) as DefaultAsset;
        getFromSmash = EditorGUILayout.Toggle("Get Data From Smash Character Data", getFromSmash, GUILayout.Width(800));
        if (getFromSmash)
        {
            smashCharName = EditorGUILayout.TextField("Smash Character Name", smashCharName);
        }
        else
        {
            dataToCopy = EditorGUILayout.ObjectField("FighterData to Copy", dataToCopy, typeof(FighterData)) as FighterData;
        }

        setupAnimatorControllerToCopy = EditorGUILayout.ObjectField("Setup Animation Controller To Copy", setupAnimatorControllerToCopy, typeof(AnimatorController)) as AnimatorController;
        portrait = EditorGUILayout.ObjectField("Portrait", portrait, typeof(Sprite)) as Sprite;
        animationClipFolder = EditorGUILayout.ObjectField("Current Animation Clip Folder", animationClipFolder, typeof(DefaultAsset)) as DefaultAsset;

        if (GUILayout.Button("Create"))
        {
            if (getFromSmash)
            {
                CreateFromSmash();
            }
            else
            {
                CreateFromFighterData();
            }
        }
        EditorGUIUtility.labelWidth = 0;
    }

    void CreateFromSmash()
    {
        FighterData fd = ScriptableObject.CreateInstance<FighterData>();
        string jsonString = EditorFileHelper.ReadFile("../Sm4shBrosCalculator/Data/" + smashCharName + "/attributes.json");
        fd.attributes = JsonUtility.FromJson<Sm4shCalculatorCharacterAttributes>(jsonString);
        fd.attributes.name = newName;
        jsonString = EditorFileHelper.ReadFile("../Sm4shBrosCalculator/Data/characters.json");
        foreach (var child in JSONObject.Create(jsonString).list)
        {
            Sm4shCalculatorCharacter characterClass = JsonUtility.FromJson<Sm4shCalculatorCharacter>(child.ToString());
            if (characterClass.Name == smashCharName || characterClass.DisplayName == smashCharName)
            {
                characterClass.DisplayName = characterClass.Name = newName;
                fd.characterClass = characterClass;
                break;
            }
        }

        jsonString = EditorFileHelper.ReadFile("../Sm4shBrosCalculator/Data/KHAPI Local/" + fd.characterClass.OwnerId.ToString() + "/moves.json");
        fd.unparsedMoves = new List<UnparsedMove>();
        fd.moves = new List<Move>();
        fd.hitboxData = new Smash_Forge.ATKD();
        foreach (var child in JSONObject.Create(jsonString).list)
        {
            UnparsedMove unparsedMove = JsonUtility.FromJson<UnparsedMove>(child.ToString());
            unparsedMove.Owner = newName;
            if (unparsedMove.OwnerId == fd.characterId)
            {
                fd.unparsedMoves.Add(unparsedMove);
                fd.moves.AddRange(Move.GetMoves(unparsedMove, fd));
            }
        }

        fd.attributes.height = fd.characterClass.Height;

        Create(fd);
    }

    void CreateFromFighterData()
    {
        Create(dataToCopy);
    }

    void Create(FighterData fd)
    {
        var basePath = AssetDatabase.GetAssetPath(newCharacterFolder);
        CreateFolderStructure();
        var fighterDataAssetPath = basePath + "/" + RESOURCES_PATH + "/" + newName + ".asset";
        if (HandleCreateFighterDataAsset(fd, basePath, fighterDataAssetPath))
        {
            HandlePortrait(fd, portrait);
            HandleSetupAnimationController(fd, basePath);
            HandleMeshAndPrefab(fd, basePath);
            MoveAnimationClips();
            SetupMaterialsAndTextures();
            AssetDatabase.Refresh();
        }
        else
        {
            Debug.LogError("Cannot create FighterData - object already exists at " + fighterDataAssetPath);
        }
    }

    bool HandleCreateFighterDataAsset(FighterData fd, string basePath, string fighterDataAssetPath)
    {
        var oldAsset = AssetDatabase.LoadAssetAtPath<FighterData>(fighterDataAssetPath);
        if (oldAsset != null)
        {
            return false;
        }

        AssetDatabase.CreateAsset(fd, fighterDataAssetPath);
        return true;
    }

    void HandleMeshAndPrefab(FighterData fd, string basePath)
    {
        if (mesh != null)
        {
            var meshPath = AssetDatabase.GetAssetPath(mesh);
            var destinationPath = basePath + "/Model/Mesh/" + newName + "." + meshPath.Split('.')[1];
            string result = "";
            if (destinationPath != meshPath)
            {
                result = AssetDatabase.MoveAsset(meshPath, destinationPath);
            }
            if (!string.IsNullOrWhiteSpace(result))
            {
                Debug.LogError("Error moving asset: " + result);
            }
            else
            {
                destinationPath = basePath + "/Prefabs/" + newName + ".prefab";
                if (!File.Exists(destinationPath))
                {
                    meshPath = AssetDatabase.GetAssetPath(mesh);
                    var modelRootGO = (GameObject)AssetDatabase.LoadMainAssetAtPath(meshPath);
                    fd.avatar = AssetDatabase.LoadAssetAtPath<Avatar>(meshPath);// avatar;
                    GameObject instanceRoot = PrefabUtility.InstantiatePrefab(modelRootGO) as GameObject;
                    instanceRoot = UpdatePrefab(instanceRoot, fd);
                    var ogModelPrefab = PrefabUtility.SaveAsPrefabAsset(instanceRoot, destinationPath);
                    fd.model = ogModelPrefab.GetComponent<FighterModel>();
                    DestroyImmediate(instanceRoot);
                }
                else
                {
                    Debug.LogError("Cannot create prefab because it already exists at path " + destinationPath);
                }
            }
        }
    }

    void HandleSetupAnimationController(FighterData fd, string basePath)
    {
        var newAnimationControllerPath = basePath + "/Animation Controllers/" + newName + " Controller [Setup].controller";
        AssetDatabase.CopyAsset(AssetDatabase.GetAssetPath(setupAnimatorControllerToCopy), newAnimationControllerPath);
        fd.setupAnimatorController = AssetDatabase.LoadAssetAtPath<RuntimeAnimatorController>(newAnimationControllerPath);
        AssetDatabase.Refresh();
        var controller = AssetDatabase.LoadAssetAtPath<AnimatorController>(newAnimationControllerPath);
        foreach (var state in SmashCharacterAnimatorTool.GetAnimatorStates(controller))
        {
            state.motion = null;
        }
    }

    void HandlePortrait(FighterData fd, Sprite portrait)
    {
        if (portrait != null)
        {
            fd.portrait = portrait;
            if (newCharacterFolder != null)
            {
                var basePath = AssetDatabase.GetAssetPath(newCharacterFolder);
                var portraitPath = AssetDatabase.GetAssetPath(portrait);
                AssetDatabase.MoveAsset(portraitPath, basePath + "/" + PORTRAIT_PATH + "/" + Path.GetFileName(portraitPath));
            }
            else
            {
                Debug.LogError("Can't move portrait: [newCharacterFolder] doesn't exist");
            }
        }
    }

    string[] GetAllFilePaths(Object asset)
    {
        string sAssetFolderPath = AssetDatabase.GetAssetPath(asset);
        // Construct the system path of the asset folder 
        string sDataPath = Application.dataPath;
        string sFolderPath = sDataPath.Substring(0, sDataPath.Length - 6) + sAssetFolderPath;
        // get the system file paths of all the files in the asset folder
        string[] aFilePaths = Directory.GetFiles(sFolderPath);
        return aFilePaths;
    }

    string[] GetAllUnityFilePaths(Object asset)
    {
        string[] ret = GetAllFilePaths(asset);
        for (int i = 0; i < ret.Length; i++)
        {
            ret[i] = ret[i].Substring(Application.dataPath.Length - 6);
        }

        return ret;
    }

    void MoveAnimationClips()
    {
        string sAssetFolderPath = AssetDatabase.GetAssetPath(animationClipFolder);
        string basePath = AssetDatabase.GetAssetPath(newCharacterFolder);
        string dataPath = Application.dataPath;
        string sFolderPath = dataPath.Substring(0, dataPath.Length - 6) + sAssetFolderPath;


        foreach (string sAssetPath in GetAllUnityFilePaths(animationClipFolder))
        {
            if (Path.GetExtension(sAssetPath) == ".fbx")
            {
                AssetDatabase.MoveAsset(sAssetPath, Path.Combine(basePath, EXPORTED_CLIPS_PATH, Path.GetFileName(sAssetPath)));
            }
        }

        bool directoryEmpty = !Directory.EnumerateFileSystemEntries(sFolderPath).Any();
        if (directoryEmpty)
        {
            AssetDatabase.DeleteAsset(sAssetFolderPath);
        }
    }

    GameObject UpdatePrefab(GameObject modelPrefab, FighterData fd)
    {
        modelPrefab.SetLayerRecursive("Hero");
        modelPrefab.AddComponent<FighterModel>();
        var animator = modelPrefab.GetComponent<Animator>();
        if (animator != null)
        {
            DestroyImmediate(animator);
        }

        Bounds b = GetGameObjectBounds(modelPrefab);

        var currentBoundsHeight = b.size.y;
        var wantedBoundsHeight = fd.attributes.height * unityAttributeHeightScale;
        modelPrefab.transform.localScale = Vector3.one * wantedBoundsHeight / currentBoundsHeight;

        GameObject shield = Instantiate(shieldPrefab);
        shield.transform.SetParent(modelPrefab.transform);
        shield.transform.localPosition = Vector3.up * b.size.y/2f;
        shield.transform.localEulerAngles = Vector3.zero;
        shield.transform.localScale = Vector3.one * b.size.x;

        var charController = modelPrefab.AddComponent<CharacterController>();
        charController.height = b.size.y;
        charController.center = Vector3.up * charController.height / 2f;

        Transform mostChildren = null;
        int maxChildCount = 0;
        foreach (Transform child in modelPrefab.transform)
        {
            if (child.childCount > maxChildCount)
            {
                mostChildren = child;
                maxChildCount = child.childCount;
            }
        }
        if (mostChildren != null)
        {
            fd.boneRoot = mostChildren.name; //Guess
            var bc = mostChildren.gameObject.AddOrGetComponent<BoxCollider>();
            bc.isTrigger = true;
            bc.size = new Vector3(1f, b.size.y, 1f);
            //TODO - automatically check if the pivot is at the bottom (Hulk) or the middle (Hermione)
            //Depending on the pivot will detrermine where the center is.
            //This is assuming the center needs to be adjusted to accomodate the pivot
            bc.center = Vector3.up * b.size.y / 2f; 
            mostChildren.gameObject.AddOrGetComponent<HitBox>();
        }

        return modelPrefab;
    }

    Bounds GetGameObjectBounds(GameObject go)
    {
        Bounds b = new Bounds();
        foreach (var r in go.GetComponentsInChildren<Renderer>())
        {
            b.Encapsulate(r.bounds);
        }

        return b;
    }

    void SetupMaterialsAndTextures()
    {
        if (mesh == null)
        {
            Debug.LogError("Can't Extract materials if [mesh] is null");
            return;
        }

        if (newCharacterFolder == null)
        {
            Debug.LogError("Can't extract materials if [newCharacterFolder] is null");
            return;
        }

        var newTextures = ExtractTexturesFromMesh();
        var newMats = ExtractMaterialsFromMesh();

        foreach (var texture in newTextures)
        {
            Debug.Log("New Texture: " + texture);
        }

        foreach(var newMat in newMats)
        {
            Debug.Log("New Material: " + newMat);
            CopyMaterialPropertiesFromSample(newMat);
        }

        if (newTextures.Count() == 1 && newMats.Count() == 1 && newMats[0].mainTexture == null)
        {
            newMats[0].mainTexture = newTextures[0];
            Debug.Log("Set material [" + newMats[0].name + "].mainTexture to [" + newTextures[0].name + "]");
        }
        else
        {
            Debug.Log("Could not automatically assign texture(s) to material(s). Please do this manually if necessary");
        }
    }

    void CopyMaterialPropertiesFromSample(Material newMat)
    {
        if (sampleMaterial != null)
        {
            newMat.CopyPropertiesFromMaterial(sampleMaterial);// = sampleMaterial.shader;

            //Remove all textures, since they probably are irrelevant
            List<Texture> allTexture = new List<Texture>();
            Shader shader = newMat.shader;
            for (int i = 0; i < ShaderUtil.GetPropertyCount(shader); i++)
            {
                if (ShaderUtil.GetPropertyType(shader, i) == ShaderUtil.ShaderPropertyType.TexEnv)
                {
                    newMat.SetTexture(ShaderUtil.GetPropertyName(shader, i), null);
                }
            }
        }
    }

    List<Texture> ExtractTexturesFromMesh()
    {
        var modelImporter = (ModelImporter)ModelImporter.GetAtPath(AssetDatabase.GetAssetPath(mesh));
        var texturesPath = AssetDatabase.GetAssetPath(newCharacterFolder) + "/" + TEXTURES_PATH;
        if (modelImporter.ExtractTextures(texturesPath))
        {
            AssetDatabase.Refresh();
            List<Texture> ret = new List<Texture>();

            foreach (string sAssetPath in GetAllUnityFilePaths(AssetDatabase.LoadAssetAtPath<Object>(texturesPath)))
            {
                if (!sAssetPath.EndsWith("meta"))
                {
                    ret.Add(AssetDatabase.LoadAssetAtPath<Texture>(sAssetPath));
                }
            }

            return ret;
        }

        return null;
    }

    //https://github.com/Unity-Technologies/UnityCsReference/blob/master/Modules/AssetPipelineEditor/ImportSettings/ModelImporterMaterialEditor.cs
    List<Material> ExtractMaterialsFromMesh()
    {
        Object[] targets = new Object[] { AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(mesh)) };
        string destinationPath = Path.Combine(AssetDatabase.GetAssetPath(newCharacterFolder), MATERIALS_PATH);
        // use the first target for selecting the destination folder, but apply that path for all targets
        /*string destinationPath = (target as ModelImporter).assetPath;
        destinationPath = EditorUtility.SaveFolderPanel("Select Materials Folder",
            FileUtil.DeleteLastPathNameComponent(destinationPath), "");
        if (string.IsNullOrEmpty(destinationPath))
        {
            // cancel the extraction if the user did not select a folder
            return;// false;
        }
        destinationPath = FileUtil.GetProjectRelativePath(destinationPath);
        */

        List<Material> ret = new List<Material>();
        try
        {
            // batch the extraction of the textures
            AssetDatabase.StartAssetEditing();

            ret = ExtractMaterialsFromAsset(targets, destinationPath);
        }
        finally
        {
            AssetDatabase.StopAssetEditing();
        }

        // AssetDatabase.StopAssetEditing() invokes OnEnable(), which invalidates all the serialized properties, so we must return.
        return ret;// true;
    }

    List<Material> ExtractMaterialsFromAsset(Object[] targets, string destinationPath)
    {
        List<string> assetPaths = new List<string>();
        var assetsToReload = new HashSet<string>();
        foreach (var t in targets)
        {
            var importer = t as AssetImporter;

            var materials = AssetDatabase.LoadAllAssetsAtPath(importer.assetPath).Where(x => x.GetType() == typeof(Material)).ToArray();
            foreach (var material in materials)
            {
                //var newAssetPath = FileUtil.CombinePaths(destinationPath, material.name) + ".mat";
                Debug.Log("new mat path: " + destinationPath + "/" + material.name);
                var newAssetPath = Path.Combine(destinationPath, material.name) + ".mat";
                newAssetPath = AssetDatabase.GenerateUniqueAssetPath(newAssetPath);
                var error = AssetDatabase.ExtractAsset(material, newAssetPath);
                if (string.IsNullOrEmpty(error))
                {
                    assetPaths.Add(newAssetPath);
                    assetsToReload.Add(importer.assetPath);
                }
            }
        }

        foreach (var path in assetsToReload)
        {
            AssetDatabase.WriteImportSettingsIfDirty(path);
            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
        }

        AssetDatabase.Refresh();
        List<Material> ret = new List<Material>();
        foreach(var path in assetPaths)
        {
            ret.Add(AssetDatabase.LoadAssetAtPath<Material>(path));
        }

        return ret;
    }

    void CreateFolderStructure()
    {

        if (newCharacterFolder == null)
        {
            return;
        }
        var basePath = AssetDatabase.GetAssetPath(newCharacterFolder);
        CreateFolder(basePath, RESOURCES_PATH);
        CreateFolder(basePath, ANIMATION_CONTROLLERS_PATH);
        CreateFolder(basePath, AUTO_UPDATED_CLIPS_PATH);
        CreateFolder(basePath, MANUALLY_EDITED_CLIPS_PATH);
        CreateFolder(basePath, EXPORTED_CLIPS_PATH);
        CreateFolder(basePath, HITBOX_ATKD_PATH);
        CreateFolder(basePath, PORTRAIT_PATH);
        CreateFolder(basePath, PREFABS_PATH);
        CreateFolder(basePath, MODEL_PATH);
        CreateFolder(basePath, MESH_PATH);
        CreateFolder(basePath, MATERIALS_PATH);
        CreateFolder(basePath,TEXTURES_PATH);
        HandleTextAssets(basePath);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="basePath"></param>
    /// <param name="folderName"></param>
    /// <param name="replaceIfDoesntExist"></param>
    /// <returns>GUID probably</returns>
    string CreateFolder(string basePath, string folderName, bool replaceIfDoesntExist = false)
    {
        string fullPath = basePath + "/" + folderName;
        if (AssetDatabase.LoadAssetAtPath<DefaultAsset>(fullPath) != null)
        {
            if (replaceIfDoesntExist)
            {
                AssetDatabase.DeleteAsset(fullPath);
                AssetDatabase.Refresh();
            }
            else
            {
                return AssetDatabase.AssetPathToGUID(fullPath);
            }
        }

        var ret = AssetDatabase.CreateFolder(basePath, folderName);
        AssetDatabase.Refresh();

        return ret;
    }

    void HandleTextAssets(string basePath)
    {
        string lowerCaseNoSpacesName = newName.Replace(" ", "_").ToLower();
        TextAsset[] assets = new TextAsset[] { readmeTemplate, packageJsonTemplate };
        foreach (var a in assets)
        {
            if (a != null)
            {
                var newPathName = a.name.Replace(".template", "");
                var newAssetText = a.text.Replace("%CharacterName%", newName);
                newAssetText = newAssetText.Replace("%LowerCaseNoSpacesCharacterName%", lowerCaseNoSpacesName);
                EditorFileHelper.WriteFile(newPathName, newAssetText);
                /*
                TextAsset newAsset = new TextAsset(newAssetText);
                AssetDatabase.CreateAsset(newAsset, basePath + "/" + newPathName);
                AssetDatabase.SaveAssets();
                */
            }
        }
    }
}